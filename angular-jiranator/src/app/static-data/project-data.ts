export const projectData = {
    project: 'PP',
    epicName: 'My Fancy Epic',
    objective: 'This is my fancy objective.\n\nIt has so many fancy things in it that people will say it\'s not even\nreal, but it totally is.  This is how you can tell:\n\nI AM FANCY MOTHERFUCKER',
    market: 'What does this look like?',
    opsTopTen: true,
    requirements: [
        {
            project: 'PP',
            workType: 'frontend',
            engineeringReady: true,
            summary: 'This is a fancy ass summary that is so sweet it hurts.',
            description: 'This is a description that goes with the fancy ass\nsummary that is so sweet it hurts.'
        },
        {
            project: 'PP',
            workType: 'backend',
            engineeringReady: false,
            summary: 'Havent finished this guy yet.',
            description: ''
        },
        {
            project: 'PP',
            workType: 'backend',
            engineeringReady: false,
            summary: 'This is another fancy ass summary that is so sweet it hurts.',
            description: 'This is another description that goes with the fancy ass\nsummary that is so sweet it hurts.'
        },
        {
            project: '',
            workType: '',
            engineeringReady: true,
            summary: '',
            description: ''
        },
    ],
    testPlan: [
        {
            summary: 'This is a fancy test plan to test how fancy the feature is.',
            description: 'You should test that the feature can actually tell me I\'m fancy or not based on my input.',
            testDate: new Date('2019-07-11T06:00:00.000Z')
        },
        {
            summary: 'Test that your mother is a hamster.',
            description: 'Since your mother is a hamster in real life, test that the software shows that to be true.',
            testDate: new Date('2019-07-12T06:00:00.000Z')
        },
        {
            summary: '',
            description: '',
            testDate: new Date('2019-07-13T06:00:00.000Z')
        },
    ],
    releaseConsiderations: 'No release considerations needed for this guy because everything\nwill totally go smoothly.'
};
