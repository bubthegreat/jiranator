
export const simpleFakeData = [
  {
    "key": "PP-751",
    "summary": "Multiple SSO Companies",
    "name": "Mercer/Boeing SSO",
    "priority": "P2",
    "status": "new",
    "issues": [
      {
        "key": "PP-755",
        "summary": "Consultants Need to Show in Their Shell Companies",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-754",
        "summary": "Multiple SSO Company Selection Page Design",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-753",
        "summary": "Multiple SSO Companies API",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-752",
        "summary": "Multiple SSO Company Selection Page Implementation",
        "priority": "P3",
        "status": "new"
      }
    ]
  },
  {
    "key": "PP-746",
    "summary": "Claims Lag Report - pushed to UI for external client use (also include a filter function)",
    "name": "Claims Lag App",
    "priority": "High",
    "status": "new",
    "issues": []
  },
  {
    "key": "PP-741",
    "summary": "Gaps In Care: Frontend experience and work",
    "name": "Gaps In Care",
    "priority": "P3",
    "status": "new",
    "issues": []
  },
  {
    "key": "PP-729",
    "summary": "Levithan Google Security Penetration Test Findings",
    "name": "Levithan Google Security Bugs",
    "priority": "P1",
    "status": "new",
    "issues": [
      {
        "key": "PP-738",
        "summary": "POSSIBLE SQL \"INJECTION\" BY MANIPULATING PREPARED STATEMENTS",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-737",
        "summary": "SPIKE: OPEN HTTP REDIRECT ON /AUTH/LOGIN",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-736",
        "summary": "S3 BUCKET WORLD READABLE",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-735",
        "summary": "CONTENT SECURITY POLICY IS NOT SET ",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-733",
        "summary": "Review how we identify superusers between frontend and backend",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-732",
        "summary": "ACCOUNT LOCKOUT CAN BE ABUSED TO DISCOVER USER PASSWORDS",
        "priority": "P4",
        "status": "new"
      },
      {
        "key": "PP-731",
        "summary": "You can PATCH/PUT to /api/users to escalate your position, modify your email and take over accounts",
        "priority": "P1",
        "status": "done"
      },
      {
        "key": "PP-730",
        "summary": "You can PATCH roles and permissions on the api/permissions endpoint without proper authorization",
        "priority": "Critical",
        "status": "done"
      }
    ]
  },
  {
    "key": "PP-665",
    "summary": "A collection of simple changes to improve the experience.",
    "name": "Job Jar Q2",
    "priority": "P3",
    "status": "new",
    "issues": [
      {
        "key": "PP-674",
        "summary": "Prevent measure/breakdown chooser from closing when click originates inside search input",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-673",
        "summary": "Indicate hidden options available for measures and breakdowns in Visualize",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-667",
        "summary": "Fix company menu selection indicator and treat sub-companies the same way",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-666",
        "summary": "Link Artemis logo to home in new nav",
        "priority": "Low",
        "status": "done"
      }
    ]
  },
  {
    "key": "PP-654",
    "summary": "Chronos 2.0: Work for Chronos 2.0 to be implemented in production (backend)",
    "name": "Chronos 2.0 Backend",
    "priority": "P3",
    "status": "new",
    "issues": []
  },
  {
    "key": "PP-653",
    "summary": "Chronos 2.0: Work for Chronos 2.0 to be implemented in production (frontend)",
    "name": "Chronos 2.0 Frontend",
    "priority": "P3",
    "status": "new",
    "issues": [
      {
        "key": "PP-762",
        "summary": "Finish initial setup and config of artemis library",
        "priority": "P3",
        "status": "indeterminate"
      },
      {
        "key": "PP-761",
        "summary": "Finish tests for artemis library (scoped to metric chooser)",
        "priority": "P3",
        "status": "indeterminate"
      },
      {
        "key": "PP-760",
        "summary": "Port over metric chooser from Chronos to Artemis Library",
        "priority": "P3",
        "status": "indeterminate"
      },
      {
        "key": "PP-759",
        "summary": "Finish building NgRx Store for Artemis Library",
        "priority": "P3",
        "status": "indeterminate"
      },
      {
        "key": "PP-758",
        "summary": "Artemis Library - Jenkins file - autorun tests and publish library",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-749",
        "summary": "Integrate artemis-library into Artemis App for Measure chooser and card",
        "priority": "P3",
        "status": "indeterminate"
      },
      {
        "key": "PP-748",
        "summary": "Integrate artemis-library into Chronos for Measure chooser and card",
        "priority": "P3",
        "status": "indeterminate"
      }
    ]
  },
  {
    "key": "PP-614",
    "summary": "UHC Provider Compliance",
    "name": "UHC Provider Compliance",
    "priority": "P3",
    "status": "new",
    "issues": [
      {
        "key": "PP-763",
        "summary": "Breakdown values of prohibited breakdowns will indicate redaction per UHC",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-624",
        "summary": "Need to investigate Standard Stories to see whether the blacklisting affects any of them",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-621",
        "summary": "Need to wholesale prevent filters on breakdowns for blacklist",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-619",
        "summary": "Analytic Advisor would have to control turning it off. Should default to on.",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PM-52",
        "summary": "Charts will display redacted text in place of breakdown values",
        "priority": "P3",
        "status": "done"
      }
    ]
  },
  {
    "key": "PP-603",
    "summary": "Dynamic Cohorts",
    "name": "Dynamic Cohorts",
    "priority": "P3",
    "status": "new",
    "issues": [
      {
        "key": "PP-609",
        "summary": "Dynamic Cohorts can be saved as a custom breakdown ",
        "priority": "P3",
        "status": "new"
      }
    ]
  },
  {
    "key": "PP-592",
    "summary": "Physician Scores & Hospital Scores",
    "name": "HEB Provider Quality",
    "priority": "P3",
    "status": "new",
    "issues": [
      {
        "key": "PP-612",
        "summary": "HEB: Navlink enhancements in physician scores",
        "priority": "Low",
        "status": "done"
      },
      {
        "key": "PP-593",
        "summary": "MSA missing",
        "priority": "P3",
        "status": "done"
      }
    ]
  },
  {
    "key": "PP-575",
    "summary": "Set up internship projects and requirements",
    "name": "Artemis Interns",
    "priority": "P3",
    "status": "new",
    "issues": []
  },
  {
    "key": "PP-572",
    "summary": "Enhancements and Support for Custom Measures ",
    "name": "Custom Measures Enhancements",
    "priority": "P3",
    "status": "new",
    "issues": []
  },
  {
    "key": "PP-421",
    "summary": "Ability to treat Sub-companies as their own Company",
    "name": "Company (de)aggregation",
    "priority": "P3",
    "status": "new",
    "issues": [
      {
        "key": "PP-824",
        "summary": "As a child company I want to compare my population to the book of business I'm in",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-823",
        "summary": "We need to know what the query looks like to get benchmarks requests for sub companies ",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-822",
        "summary": "Company needs to be in place of sub company everywhere like in the admin and in the dropdown navigation",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-821",
        "summary": "make sure roles and permissions don't break with the new subcompanies",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-820",
        "summary": "need to link the new sub companies to their current metrics in airtable ",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-819",
        "summary": "need to recreate all currently live sub companies in airtable",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-818",
        "summary": "Airtable needs to have the new parent company id ",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-817",
        "summary": "remove current sub company from session",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-816",
        "summary": "make sure that adding sub companies to users in the Artemis admin still works ",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-815",
        "summary": "update the current user end point to point to new companies instead of subcompanies",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-814",
        "summary": "remove all branch code for subcompanies",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-813",
        "summary": "make sure benchmark requests work",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-812",
        "summary": "ensure metric sync works",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-811",
        "summary": "Need a data migration",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-810",
        "summary": "Need to do a schema migration ",
        "priority": "P3",
        "status": "new"
      }
    ]
  },
  {
    "key": "PP-359",
    "summary": "Benchmarks for V2 (Right after Elevate)",
    "name": "Benchmarks V2",
    "priority": "P3",
    "status": "new",
    "issues": [
      {
        "key": "PP-660",
        "summary": "Ensure that selected service category is shown when moving from Trend Explorer --> Explore",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-659",
        "summary": "Graceful failure when there is a background or browser issue",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-658",
        "summary": "Clarify which column is sorted",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-657",
        "summary": "Link directly to explanations of loosely, moderately, and well managed",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-629",
        "summary": "Prescriptions are missing from the dropdown list inside the service category level 3 filter chooser",
        "priority": "Medium",
        "status": "done"
      },
      {
        "key": "PP-611",
        "summary": "Link top 5 impactable trends to Explore in Trend Explorer",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-597",
        "summary": "Permissions - With Milliman Benchmarks Permission Turned Off The Benchmarks Dropdown Is Still Available in Standard Stories",
        "priority": "Critical",
        "status": "done"
      },
      {
        "key": "PP-589",
        "summary": "Adding Trend Charts To Explorations Lots of Overlap",
        "priority": "Medium",
        "status": "done"
      },
      {
        "key": "PP-586",
        "summary": "As a user coming back to Trend Explorer after clicking into Explore, I can see all the Setup and Filter changes I made before",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-570",
        "summary": "Explorations - Sparkline Gets Pushed Down When Filter Applied",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-564",
        "summary": "Make trend explorer med/rx calculation based on separate values rather than rollup",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-561",
        "summary": "Visualize - Adding More Measures Not Working",
        "priority": "P1",
        "status": "done"
      },
      {
        "key": "PP-557",
        "summary": "Visualizations - Benchmarks Warning Displayed When Nothing Regarding Benchmarks Was Chosen",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-556",
        "summary": "Trend explore hyperlink defaults to CCS level 2 breakdown and not HCG service level 3",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-555",
        "summary": "Add percent change arrow to trend drivers card for percentage change to match elsewhere",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-550",
        "summary": "Trend Explorer: \"Top Impactable Trend Drivers\" Card > add \"v Util Benchmark\" column so all three components of the selection criteria are visible (and since we have more room now)",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-549",
        "summary": "Add links to service category in \"top impactable trend drivers\" card https://artemis-health.slack.com/archives/CG8AN4ENP/p1556381331131100",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-548",
        "summary": "Trend Explorer - When at the bottom of the first page, and then go to next page you remain at the bottom",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-547",
        "summary": "Trend Explorer - Utilization Trend for Preventive Care showing an increase in red. These are good things so they should be green",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-546",
        "summary": "Visualizations - Have the ability to pull the benchmark value into a value card visualization in a story",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-541",
        "summary": "Measure Screen - Custom Measures and Milliman. If you have a lot of custom measures, there is no scroll bar in the benchmark dropdown. MMA is prime example.",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-539",
        "summary": "Custom Benchmarks Titles Have A Length of 150 Characters. If you use all 150 characters The benchmark values are pushed completely out of the grey bar in the exploration. ",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-533",
        "summary": "Exploration - Benchmark on the Sparkline missing",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-532",
        "summary": "Benchmarks are off screen with certain comparisons",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-527",
        "summary": "Export. to CSV doesn't show benchmark",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-525",
        "summary": "Standard Stories: Executive Summary & Pharmacy default benchmarks to moderately managed on page load",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-523",
        "summary": "Explore App: If you add a measure, then add a benchmark, then go into the measure chooser and select another measure, the dark grey box from the previous benchmark remains on the screen",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-522",
        "summary": "Safari: Chart in trend explorer is grey and not yellow, red and green",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-521",
        "summary": "Measure picker: Count of measures should drop to actual number in the list when hiding measures without benchmarks",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-520",
        "summary": "[Lower priority] Trend explorer: add solid line to the side of the sticky column",
        "priority": "P4",
        "status": "done"
      },
      {
        "key": "PP-519",
        "summary": "[Not release blocker ] Trend explorer: If you select bubbles on the chart, then add a filter from either of the other two places, the chart redraws and the selected bubbles are no longer blue",
        "priority": "P4",
        "status": "done"
      },
      {
        "key": "PP-515",
        "summary": "Explore: I get 3 loading box \"spinners\" in one explore summary card when loading new data",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-514",
        "summary": "Export CSV date range doesn't match the date range selected in app (Scott doesn't think benchmarks V1 release blocker)",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-513",
        "summary": "If you modify two benchmarks in quick succession, the \"autosave\" boots out my changes to one of the two items and only keeps one.  ",
        "priority": "P4",
        "status": "done"
      },
      {
        "key": "PP-510",
        "summary": "Changing anything the loading of the trend explorer is very slow",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-507",
        "summary": "In explore, benchmarks with a long custom name have text that spills over as it wraps",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-499",
        "summary": "Standard Stories - Benchmark Not Showing In Downloaded PDF",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-485",
        "summary": "IE 11 positive benchmarks are showing an up and down arrow:",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-484",
        "summary": "IE11 performance. When clicking the ellipsis on a measure to get the drop down it is a good 5 seconds or more before the dropdown appears. ",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-482",
        "summary": "The loading time between switching from the custom benchmarks app to other areas of the application is too long. It is even worse in IE 11",
        "priority": "P2",
        "status": "done"
      },
      {
        "key": "PP-480",
        "summary": "IE 11 Standard Stories loading icons still spinning after everything has loaded: ",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-479",
        "summary": "IE11 Exploration with Milliman and Custom benchmark The custom benchmark is showing n/a even though the milliman benchmark is chosen. When benchmarks are chosen the loading spinner just stays and never goes away.",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-478",
        "summary": "IE11 Exploration still showing loading spinner in after several minutes",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-475",
        "summary": "Benchmark label is under the line chart: ",
        "priority": "Medium",
        "status": "done"
      },
      {
        "key": "PP-470",
        "summary": "looks like the benchmark color is using opacity which makes it look different when there's a bar behind. Can we pick a solid color that will look the same on or off the bar?",
        "priority": "P4",
        "status": "done"
      },
      {
        "key": "PP-455",
        "summary": "Validate the \"can't apply custom benchmarks\" list for frontend",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-451",
        "summary": "Users should be able to export CSV with benchmark information",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-397",
        "summary": "Benchmarks V1: Benchmarks should be available in the header of the PDF download",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-382",
        "summary": "Benchmarks V2: Apps using benchmarks should not run noticeably slower (add > 1 second) than running without benchmarks",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-358",
        "summary": "Benchmarks: Support Breakdowns",
        "priority": "P3",
        "status": "new"
      }
    ]
  },
  {
    "key": "PP-313",
    "summary": "Rework Actionable Overspending Book Of Business to a maintainable state",
    "name": "Artemis BoB Benchmarks",
    "priority": "P3",
    "status": "new",
    "issues": [
      {
        "key": "PP-334",
        "summary": "Artemis Bob benchmarks back end work for calculating and managing values",
        "priority": "P2",
        "status": "new"
      },
      {
        "key": "PP-333",
        "summary": "Artemis BoB benchmarks front end work for managing values",
        "priority": "P2",
        "status": "new"
      }
    ]
  },
  {
    "key": "PP-301",
    "summary": "Remove Medical Claims Table",
    "name": "Remove Medical Claims Table",
    "priority": "P4",
    "status": "new",
    "issues": [
      {
        "key": "PP-422",
        "summary": "Validation Plan",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-341",
        "summary": "Remove all the med app based permissions",
        "priority": "Low",
        "status": "done"
      },
      {
        "key": "PP-332",
        "summary": "Remember to remove med link from chronos",
        "priority": "Medium",
        "status": "done"
      },
      {
        "key": "PP-283",
        "summary": "Medical Claim App appears to be duplicated records",
        "priority": "P2",
        "status": "done"
      }
    ]
  },
  {
    "key": "PP-273",
    "summary": "Support having clauses in the app",
    "name": "Having Clause ",
    "priority": "P3",
    "status": "new",
    "issues": []
  },
  {
    "key": "PP-240",
    "summary": "Generate 1,000+ reports for clients at one time",
    "name": "Kentuckiana",
    "priority": "P4",
    "status": "indeterminate",
    "issues": [
      {
        "key": "PP-826",
        "summary": "Add labels to slider for 0 and 100",
        "priority": "P3",
        "status": "indeterminate"
      },
      {
        "key": "PP-825",
        "summary": "Change bottom 10% on slider to bottom 25% and adjust the size of the slider accordingly",
        "priority": "P3",
        "status": "indeterminate"
      },
      {
        "key": "PP-769",
        "summary": "Put in blanks and hyphens in the Medicaid/Commercial Columns instead of 0 and 0% in the cases where they do not have Medicaid/Commercial data",
        "priority": "P3",
        "status": "indeterminate"
      },
      {
        "key": "PP-768",
        "summary": "Change \u201cPercent above/below average\u201d to \u201cMeasures above/below average\u201d on the 2nd and 3rd cards",
        "priority": "P3",
        "status": "indeterminate"
      },
      {
        "key": "PP-767",
        "summary": "Remove the previous year column on the \"your score compared to\" section of the individual report",
        "priority": "P3",
        "status": "indeterminate"
      },
      {
        "key": "PP-766",
        "summary": "Add NPI under the provider name on the individual report",
        "priority": "P3",
        "status": "indeterminate"
      },
      {
        "key": "PP-636",
        "summary": "the two overview boxes are the same template as the left and right overview boxes on the group report.",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-635",
        "summary": "the bottom paragraph matches May 13 mockup.",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-634",
        "summary": "each table row contains pill bar on top of 0-100 line with \u201cbottom 10%\u201c and \u201ctop 25% benchmark\u201d value displayed on each end with \u201cyour practice\u201d plotted between and a legend in table head above with labels.",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-633",
        "summary": "the right overview box shows bottom 2 measures by percent below average with percent and measure name.",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-632",
        "summary": "the center overview box shows top 3 measures by percent above average with percent and measure name.",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-631",
        "summary": "the left overview box shows total count of measures on report, number above average, and number performing better than benchmark with Benchmarks Provider ribbon icon.",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-630",
        "summary": "the top paragraph matches May 13 mockup.",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-241",
        "summary": "Kentuckiana PDF reports",
        "priority": "P2",
        "status": "indeterminate"
      }
    ]
  },
  {
    "key": "PP-24",
    "summary": "Single Sign On with Google",
    "name": "Google SSO",
    "priority": "P3",
    "status": "done",
    "issues": [
      {
        "key": "PP-765",
        "summary": "Google SSO: Unhide login button on home page",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-764",
        "summary": "Hide google SSO button from prod for now",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-715",
        "summary": "Google SSO: Create end to end tests",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-714",
        "summary": "Google SSO: Caching/Cookie",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-713",
        "summary": "Google SSO: Configurations should be separate for the various environments",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-712",
        "summary": "Google SSO: Redirect to sign in page when login doesn't exist for google SSO",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-711",
        "summary": "Google SSO: Sign in button on front page",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-710",
        "summary": "Google SSO: Sign out portal redirect if configured",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-708",
        "summary": "Google SSO: Ability to logout",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-707",
        "summary": "Google SSO: Convert normal users to SSO users",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-706",
        "summary": "Google SSO: Skip username only screen for non-sso users",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-705",
        "summary": "Google SSO: Forgot/Change password URL link when configured",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-704",
        "summary": "Google SSO: SSO Error page redirect on error",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-702",
        "summary": "Google SSO: No redirect to SSO after first login",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-701",
        "summary": "Google SSO: Redirect to SSO as configured if enabled",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-700",
        "summary": "Google SSO: Identify users as SSO during after creation; As an admin, I should be able to toggle an on/off button for SSO for my company",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-698",
        "summary": "Google SSO: Assertion Consumer Service URL",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-697",
        "summary": "Google SSO: Auto-redirect SSO custom link configuration",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-696",
        "summary": "Google SSO: Artemis identity provider URL",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-695",
        "summary": "Google SSO: Metadata XML file and link to XML metadata available in admin",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-694",
        "summary": "Google SSO: Provide key information for SSO admin",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-693",
        "summary": "Google SSO: Give a failure message when the test failson the admin page",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-692",
        "summary": "Google SSO: Give a success message when the test succeeds on the admin page",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-691",
        "summary": "Google SSO: Admin should be able to test the connection from the setup area when all required fields are filled out - test should include testing URLs to make sure they are valid and that the SSO connection is successful",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-690",
        "summary": "Google SSO: Upload Certificate - required",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-689",
        "summary": "Google SSO: Secure Hash (SHA-1, SHA-2, SHA-256, or SHA-512) - required (defaults to SHA-256)",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-688",
        "summary": "Google SSO: SSO Error Page URL",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-687",
        "summary": "Google SSO: Forgot/Change Password URL",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-686",
        "summary": "Google SSO: Sign-Out Portal URL",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-685",
        "summary": "Google SSO: Log-In Portal URL",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-684",
        "summary": "Google SSO: Identity Provider Metadata XML - required",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-683",
        "summary": "Google SSO: Binding Type (Redirect, Post, SOAP, or Artifact) - required (defaults to Redirect)",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-682",
        "summary": "Google SSO: Admin area should allow admins to enter data required to authenticate Google users",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-681",
        "summary": "As an artemis employee, I can remove the SSO capability, and if it's disabled, this functionality won't show up anywhere.",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-679",
        "summary": "As a user, it should not matter if my account was created manually or I used my google SSO account - they should inherit the same functionality from configuration at the company level, etc.",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-678",
        "summary": "As an admin, if I go to the setup area, I can select google as a global setting.  This would be the default if I navigate there.",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-677",
        "summary": "As an admin, I should be able to toggle an on/off button for SSO for my company",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-201",
        "summary": "[Spec] Single Sign On with Google",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-76",
        "summary": "SPIKE: \"Test SSO\" Endpoint",
        "priority": "P3",
        "status": "new"
      }
    ]
  },
  {
    "key": "PP-12",
    "summary": "Program Stories in app",
    "name": "Program Stories",
    "priority": "P3",
    "status": "new",
    "issues": [
      {
        "key": "PP-415",
        "summary": "Program Story Visuals",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-414",
        "summary": "Program story measures",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-413",
        "summary": "Impact Calculator ",
        "priority": "P3",
        "status": "new"
      }
    ]
  },
  {
    "key": "PP-9",
    "summary": "Joins in app",
    "name": "Joins",
    "priority": "P3",
    "status": "new",
    "issues": [
      {
        "key": "PP-602",
        "summary": "UX to test the summary # at the top as the aggregation of the rows vs. total dollars including nulls",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-601",
        "summary": "By default nulls are excluded from the summary measure and the breakdown measure",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-600",
        "summary": "break any custom measure down by any breakdown",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-599",
        "summary": "Break any measure down by any breakdown",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-598",
        "summary": "Data should pull from it's source table every time ",
        "priority": "P3",
        "status": "new"
      }
    ]
  },
  {
    "key": "PP-7",
    "summary": "Add condition stories to SS",
    "name": "Standard Stories R2",
    "priority": "P4",
    "status": "new",
    "issues": [
      {
        "key": "PP-231",
        "summary": "Change HCC PMPM box to Average Med Paid per HCC",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-213",
        "summary": "Angular Line Chart Component",
        "priority": "P3",
        "status": "done"
      },
      {
        "key": "PP-208",
        "summary": "Condition story top section",
        "priority": "P4",
        "status": "new"
      },
      {
        "key": "PP-207",
        "summary": "Condition Standard Stories List view",
        "priority": "P4",
        "status": "new"
      },
      {
        "key": "PP-182",
        "summary": "Design and implementation for SS when a single (or multiple) requests fail ",
        "priority": "P4",
        "status": "new"
      },
      {
        "key": "PP-179",
        "summary": "Opiod Use Standard Story (METRICS NOT IN ZEUS)",
        "priority": "P4",
        "status": "new"
      },
      {
        "key": "PP-177",
        "summary": "Standard Stories Phase 2",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-165",
        "summary": "Maternity Standard Story",
        "priority": "P4",
        "status": "new"
      },
      {
        "key": "PP-162",
        "summary": "Diabetes Standard Story",
        "priority": "P4",
        "status": "new"
      },
      {
        "key": "PP-161",
        "summary": "Behavioral Health Standard Story",
        "priority": "P4",
        "status": "new"
      },
      {
        "key": "PP-160",
        "summary": "Musculoskeletal Standard Story",
        "priority": "P4",
        "status": "new"
      },
      {
        "key": "PP-159",
        "summary": "Elective Surgery Standard Story",
        "priority": "P4",
        "status": "new"
      },
      {
        "key": "PP-158",
        "summary": "Cardiovascular Standard Story",
        "priority": "P4",
        "status": "new"
      },
      {
        "key": "PP-157",
        "summary": "Neoplasms Standard Story",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-22",
        "summary": "Behavioral Health Services Research",
        "priority": "High",
        "status": "done"
      },
      {
        "key": "PP-21",
        "summary": "Musculoskeletal Procedures Research",
        "priority": "High",
        "status": "done"
      },
      {
        "key": "PP-20",
        "summary": "Sign off on Mocks",
        "priority": "P3",
        "status": "new"
      },
      {
        "key": "PP-10",
        "summary": "Ability to download multiple Standard Stories at once ",
        "priority": "P3",
        "status": "new"
      }
    ]
  }
]