import { Component, OnInit, Input } from '@angular/core';
import { Epic, Jira } from '../../interfaces/jira.interface';

@Component({
  selector: 'app-engineering-ready-status',
  templateUrl: './engineering-ready-status.component.html',
  styleUrls: ['./engineering-ready-status.component.scss']
})
export class EngineeringReadyStatusComponent implements OnInit {

  @Input() epic: Epic;

  readinessStatus: string;
  statusColor: string;
  statusReason: string;

  constructor() { }

  ngOnInit() {
    this.readinessStatus = this.epic.readinessStatus;
    this.statusColor = this.epic.statusColor;
    this.statusReason = this.epic.statusReason;
  }

}
