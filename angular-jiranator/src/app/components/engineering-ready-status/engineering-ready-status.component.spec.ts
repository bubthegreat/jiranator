import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineeringReadyStatusComponent } from './engineering-ready-status.component';

describe('EngineeringReadyStatusComponent', () => {
  let component: EngineeringReadyStatusComponent;
  let fixture: ComponentFixture<EngineeringReadyStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngineeringReadyStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineeringReadyStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
