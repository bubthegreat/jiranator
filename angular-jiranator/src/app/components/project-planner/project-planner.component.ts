import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { JiranatorService } from 'src/app/services/jiranator-service.service';
import { ActivatedRoute } from '@angular/router';
import { Project } from 'src/app/interfaces/project.interface';
import { MatSnackBar } from '@angular/material/snack-bar';


const projects: Array<any> = [
  { value: 'PP', niceName: 'Artemis' },
  { value: 'ZEUS', niceName: 'Zeus' },
  { value: 'AN', niceName: 'Analytics' },
  { value: 'OPS', niceName: 'Operations' },
];

const noWeekendFilter = (d: Date): boolean => {
  const day = d.getDay();
  // Prevent Saturday and Sunday from being selected.
  return day !== 0 && day !== 6;
};

const workTypes: Array<any> = [
  { value: 'backend', niceName: 'Back End' },
  { value: 'frontend', niceName: 'Front End' },
  { value: 'other', niceName: 'Other' },
];

@Component({
  selector: 'app-project-planner',
  templateUrl: './project-planner.component.html',
  styleUrls: ['./project-planner.component.scss']
})
export class ProjectPlannerComponent implements OnInit, OnDestroy {
  isLinear = true;
  minDesLen = 64;
  minSumLen = 32;
  maxSumLen = 128;
  projects = projects;
  workTypes = workTypes;
  maxEpicNameLen = 32;
  projectForm: FormGroup;
  formSubscription: Subscription;
  numValidRequirements = 0;
  numValidTestPlans = 0;
  numBackend = 0;
  numFrontend = 0;
  numOther = 0;
  projectId: string;
  dateFilter = noWeekendFilter;

  workTypeResults: any[] = [
    { name: 'Backend', value: this.numBackend },
    { name: 'Frontend', value: this.numFrontend },
    { name: 'Other', value: this.numOther }
  ];

  validRequirementResults: any[] = [
    { name: 'Invalid Requirements', value: 0, },
    { name: 'Valid Requirements', value: 1, },
  ];

  validTestPlanResults: any[] = [
    { name: 'Invalid Test Plans', value: 0, },
    { name: 'Valid Test Plans', value: 1, },
  ];

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private jiranatorService: JiranatorService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.projectForm = this.createForm();
    this.route.params.subscribe(routeParams => {
      console.log(routeParams);
      if (routeParams.projectId) {
        const projectId = this.route.snapshot.paramMap.get('projectId');
        console.log('I got a route for projectId: ', projectId );
        this.projectId = projectId;
        this.jiranatorService.loadProject(this.projectId)
          .subscribe(formData => {
            console.log('Patching formdata: ', formData);
            this.loadData(JSON.parse(formData));
          });
      }
    });
    // Set the number of requirements and test plans that are valid.
    this.updateData();

    this.formSubscription = this.projectForm.valueChanges.pipe(debounceTime(3000))
      .subscribe(formResult => {
        // Clean up workType values if they've changed from PP
        formResult.requirements.forEach(element => {
          if (element.project !== 'PP') {
            element.workType = '';
          }
        });
        if (this.projectId == null) {
          console.log('No project ID found - creating new project.');
          this.createProject();
        } else {
          console.log('Project found - updating project.');
          this.jiranatorService.updateProject(this.projectId, this.projectForm.value)
            .subscribe( () => { this.snackIt('Changes saved.');});
        }
        this.updateData();
      });
  }

  ngOnDestroy() {
    this.formSubscription.unsubscribe();
  }

  loadData(formData: Project) {
    // Patch base data
    this.projectForm.patchValue(formData);
    // Patch all requirements.
    formData.requirements.forEach((requirement, index) => {
      const reqFormGroup = this.createRequirement();
      reqFormGroup.patchValue(requirement);
      this.requirements.push(reqFormGroup);
    });

    // Patch all test plans
    formData.testPlan.forEach(testPlanItem => {
      const testFormGroup = this.createTestPlanItem();
      testFormGroup.patchValue(testPlanItem);
      this.testPlan.push(testFormGroup);
    });
    // Update the data in the form control.
    this.updateData();
    this.snackIt('Project loaded.');
  }

  updateData() {
    this.getRequirementTypes();
    this.numValidRequirements = this.getValidFormArrayItems(this.requirements.controls);
    this.numValidTestPlans = this.getValidFormArrayItems(this.testPlan.controls);
    this.buildWorkTypeResults();
    this.buildValidRequirementResults();
    this.buildValidTestPlanResults();
  }

  customColors = (name) => {
    let color = 'r#f0165bed';
    if (name === 'Invalid Requirements') {
      color = '#f0165b';
    } else if (name === 'Invalid Test Plans') {
      color = '#f0165b';
    } else if (name === 'Valid Requirements') {
      color = '#6fe060';
    } else if (name === 'Valid Test Plans') {
      color = '#6fe060';
    }
    return color;
  }

  buildWorkTypeResults() {
    const tempResults: any[] = [
      { name: 'Backend Stories', value: this.numBackend },
      { name: 'Frontend Stories', value: this.numFrontend },
      { name: 'Other Stories', value: this.numOther },
    ];
    this.workTypeResults = tempResults;
  }

  buildValidRequirementResults() {
    const numInvalidRequirements = this.projectForm.value.requirements.length - this.numValidRequirements;
    const tempResults: any[] = [
      { name: 'Valid Requirements', value: this.numValidRequirements, },
      { name: 'Invalid Requirements', value: numInvalidRequirements, },
    ];
    this.validRequirementResults = tempResults;
  }

  buildValidTestPlanResults() {
    const numValidTestPlanResults = this.projectForm.value.testPlan.length - this.numValidTestPlans;
    const tempResults: any[] = [
      { name: 'Valid Test Plans', value: this.numValidTestPlans, },
      { name: 'Invalid Test Plans', value: numValidTestPlanResults, },
    ];
    this.validTestPlanResults = tempResults;
  }

  // Get the number of valid requirements from the array of requirements
  getValidFormArrayItems(requirementsControls) {
    let validRequirements = 0;
    requirementsControls.forEach(element => {
      if (element.status === 'VALID') {
        validRequirements += 1;
      }
    });
    return validRequirements;
  }

  // Get the requirement types from the form result.
  getRequirementTypes() {
    let backendReqs = 0;
    let frontendReqs = 0;
    let otherReqs = 0;

    // Count the number of each type of requirement.
    this.projectForm.value.requirements.forEach(requirement => {
      if (requirement.workType === 'frontend') {
        frontendReqs += 1;
      } else if (requirement.workType === 'backend') {
        backendReqs += 1;
      } else {
        otherReqs += 1;
      }
    });
    // Set the requirements types to their counted numbers
    this.numBackend = backendReqs;
    this.numFrontend = frontendReqs;
    this.numOther = otherReqs;
  }

  // Parse a display string from the requirement data.
  parseRequirementDisplay(requirement) {
    let returnString = requirement.value.project;
    if (requirement.value.workType === 'frontend') {
      returnString += ' (frontend)';
    } else if (requirement.value.workType === 'backend') {
      returnString += ' (backend)';
    }
    return returnString;
  }

  createForm() {
    return this.formBuilder.group({
      epicName: ['', [Validators.required, Validators.maxLength(this.maxEpicNameLen)]],
      project: ['', Validators.required],
      dueDate: ['', Validators.required],
      stakeholders: ['', Validators.required],
      objective: ['', [Validators.minLength(this.minDesLen), Validators.required]],
      requirements: this.formBuilder.array([]),
      testPlan: this.formBuilder.array([]),
      releaseConsiderations: ['', [Validators.minLength(this.minDesLen), Validators.required]],
    });
  }

  // Create a requirement form group.
  createRequirement() {
    const group = this.formBuilder.group({
      project: ['', Validators.required],
      workType: [''],
      storyPoints: ['', Validators.required],
      summary: ['', [Validators.minLength(this.minSumLen), Validators.maxLength(this.maxSumLen), Validators.required]],
      description: ['', [Validators.minLength(this.minDesLen), Validators.required]],
    });
    group.patchValue({storyPoints: null});
    return group;
  }

  // Create a test plan form group.
  createTestPlanItem() {
    return this.formBuilder.group({
      summary: ['', [Validators.minLength(this.minSumLen), Validators.maxLength(this.maxSumLen), Validators.required]],
      description: ['', [Validators.minLength(this.minDesLen), Validators.required]],
      testDate: [''],
    });
  }

  removeRequirement(index) {
    this.requirements.removeAt(index);
  }

  removeTestDate(index) {
    this.testPlan.removeAt(index);
  }

  addRequirement() {
    this.requirements.push(this.createRequirement());
  }

  addTestDate() {
    this.testPlan.push(this.createTestPlanItem());
  }

  createProject() {
    this.jiranatorService.createProject(this.projectForm.value)
      .subscribe( projectId => {
        console.log('projectId: ', projectId);
        window.history.replaceState({}, '', `/project/${projectId}`);
        this.projectId = projectId;
      });
    this.snackIt('Project created.');
    }

  createJiras() {
    if (this.projectForm.valid) {
      this.snackIt('Creating Jiras...');
    } else {
      this.snackIt('Error creating Jiras.');
    }
  }

  snackIt(snackMessage: string) {
    this.snackBar.open(snackMessage, '', {duration: 1000});
  }

  get project() {
    return this.projectForm.get('project') as FormGroup;
  }

  get epicName() {
    return this.projectForm.get('epicName') as FormGroup;
  }

  get objective() {
    return this.projectForm.get('objective') as FormGroup;
  }

  get market() {
    return this.projectForm.get('market') as FormGroup;
  }

  get opsTopTen() {
    return this.projectForm.get('opsTopTen') as FormGroup;
  }

  get requirements() {
    return this.projectForm.get('requirements') as FormArray;
  }

  get testPlan() {
    return this.projectForm.get('testPlan') as FormArray;
  }

  get releaseConsiderations() {
    return this.projectForm.get('releaseConsiderations') as FormGroup;
  }

}
