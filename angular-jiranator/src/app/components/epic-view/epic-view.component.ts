import { Component, OnInit, Input, ViewChild, AfterViewInit, OnChanges, OnDestroy } from '@angular/core';
import { JiranatorService } from '../../services/jiranator-service.service';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { Epic } from '../../interfaces/jira.interface';
import { MatSort, MatSortable } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { EpicStatusBarComponent } from '../epic-status-bar/epic-status-bar.component';
import { EngineeringReadyStatusComponent } from '../engineering-ready-status/engineering-ready-status.component';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-epic-view',
  templateUrl: './epic-view.component.html',
  styleUrls: ['./epic-view.component.scss']
})
export class EpicViewComponent implements OnInit, OnDestroy {
  @Input() projectKey: string;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(EpicStatusBarComponent, {static: true}) private statusBar: EpicStatusBarComponent;
  @ViewChild(EngineeringReadyStatusComponent, {static: true}) private readyStatus: EngineeringReadyStatusComponent;

  isLoading = false;
  displayedColumns: string[] = ['key', 'priority', 'name', 'summary', 'issue-count', 'status-bar', 'due-date', 'engineering-ready'];
  epics: Epic[];
  projectKey$: Subscription;
  dataSource: MatTableDataSource<Epic> = new MatTableDataSource<Epic>([]);

  constructor(
    private jiranatorService: JiranatorService,
    private route: ActivatedRoute,
    ) { }

  ngOnInit() {
    console.log('Running the getProjectEpics function');
    this.projectKey$ = this.route.params.subscribe(routeParams => {
      console.log('Route param changed: ', routeParams);
      this.loadEpicInfo(routeParams.projectKey);
    });


    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = (epic, property) => {
      switch (property) {
        case 'issue-count': return epic.length;
        case 'status-bar': return epic.toDoNumber;
        case 'engineering-ready': return epic.readinessStatus;
        case 'due-date': return new Date(epic.dueDate);
        default: return epic[property];
      }
    };
    this.sort.sort(({ id: 'engineering-ready', start: 'desc'}) as MatSortable);
  }

  ngOnDestroy(): void {
    this.projectKey$.unsubscribe();
  }

  loadEpicInfo(projectKey: string) {
    console.log('Getting data for new projectKey: ', projectKey);
    const epics$: Observable<Epic[]> = this.getProjectEpics(projectKey);
    epics$.subscribe(
      epics => {
        this.isLoading = false;
        this.epics = epics;
        this.dataSource.data = epics;
        console.log(this.projectKey + ' epics: ');
        console.log(epics);
    });
  }

  getProjectEpics(projectKey: string): Observable<any> {
    return this.jiranatorService.getProjectEpicsAndJiras(projectKey);
    //return this.jiranatorService.getFakeData(projectKey);
  }
}
