import { Component, OnInit } from '@angular/core';
import { JiranatorService } from 'src/app/services/jiranator-service.service';
import { Project } from 'src/app/interfaces/project.interface';

@Component({
  selector: 'app-list-projects',
  templateUrl: './list-projects.component.html',
  styleUrls: ['./list-projects.component.scss']
})
export class ListProjectsComponent implements OnInit {
  projects: Array<any>;
  displayedColumns: string[] = ['Name', 'Objective'];

  constructor(private jiranatorService: JiranatorService) { }

  ngOnInit() {
    this.jiranatorService.getAllProjects()
      .subscribe(projectArray => {
        const allProjects = [];
        projectArray.forEach(( projectData: string) => {
          allProjects.push(
            {projectId: projectData[0], projectObject: JSON.parse(projectData[1])}
          );
        });
        this.projects = allProjects;
        console.log(this.projects);
      });

  }

}
