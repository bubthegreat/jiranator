import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpicStatusBarComponent } from './epic-status-bar.component';

describe('EpicStatusBarComponent', () => {
  let component: EpicStatusBarComponent;
  let fixture: ComponentFixture<EpicStatusBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpicStatusBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpicStatusBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
