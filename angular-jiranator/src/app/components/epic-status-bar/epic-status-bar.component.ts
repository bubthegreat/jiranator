import { Component, OnInit, Input } from '@angular/core';
import { Epic, Jira } from '../../interfaces/jira.interface';

@Component({
  selector: 'app-epic-status-bar',
  templateUrl: './epic-status-bar.component.html',
  styleUrls: ['./epic-status-bar.component.scss']
})
export class EpicStatusBarComponent implements OnInit {
  @Input() epic: Epic;
  toDoPercent: string;
  inProgressPercent: string;
  donePercent: string;
  otherPercent: string;

  constructor() { }

  ngOnInit() {
    this.parseEpicStatus();
  }

  getPercent(issueNumber: number) {
    return String(100 * issueNumber / this.epic.length) + '%';
  }

  parseEpicStatus() {
    this.toDoPercent = this.getPercent(this.epic.toDoNumber);
    this.inProgressPercent = this.getPercent(this.epic.inProgressNumber);
    this.donePercent = this.getPercent(this.epic.doneNumber);
    this.otherPercent = this.getPercent(this.epic.otherNumber);
  }



}
