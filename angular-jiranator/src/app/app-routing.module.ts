import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectPlannerComponent } from './components/project-planner/project-planner.component';
import { AppComponent } from './app.component';
import { EpicViewComponent } from './components/epic-view/epic-view.component';
import { ListProjectsComponent } from './components/list-projects/list-projects.component';

const routes: Routes = [
  { path: 'project/new', component: ProjectPlannerComponent, },
  { path: 'project/:projectId', component: ProjectPlannerComponent, },
  { path: 'project', component: ListProjectsComponent, },
  { path: 'epics/:projectKey', component: EpicViewComponent, },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  bootstrap: [AppComponent],
})
export class AppRoutingModule { }
