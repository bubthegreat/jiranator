import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Epic } from '../interfaces/jira.interface';
import { Project } from '../interfaces/project.interface';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class JiranatorService {
  private apiUrl = 'http://localhost:8000';

  constructor(private http: HttpClient) { }

  getProjectEpicsAndJiras(projectKey: string): Observable<Epic[]> {
    const urlString = this.apiUrl + '/epics-and-jiras/' + projectKey;
    console.log('Trying to run the api call to: ' + urlString);
    return this.http.get<Epic[]>(urlString);
  }

  getFakeData(projectKey: string): Observable<Epic[]> {
    const urlString = this.apiUrl + '/fake-epics-and-jiras/' + projectKey;
    console.log('Trying to run the api call to: ' + urlString);
    return this.http.get<Epic[]>(urlString);
  }

  createProject(formData): Observable<any> {
    const urlString = this.apiUrl + '/new-project/';
    console.log('Trying to run the api call to: ' + urlString);
    console.log('Saving projectData: ', formData);
    return this.http.post(urlString, formData, httpOptions);
  }

  updateProject(projectId, formData): Observable<any> {
    const urlString = this.apiUrl + '/project/' + projectId;
    console.log('Trying to run the api call to: ' + urlString);
    console.log('Updating projectData with current data: ', formData);
    return this.http.post(urlString, formData, httpOptions);
  }

  loadProject(projectId: string): Observable<any> {
    console.log('Loading project: ', projectId);
    const urlString = this.apiUrl + '/project/' + projectId;
    console.log('Trying to run the api call to: ' + urlString);
    const projectInfo = this.http.get<Project>(urlString);
    return projectInfo;
  }

  getAllProjects(): Observable<any> {
    console.log('Getting all projects.');
    const urlString = this.apiUrl + '/project/';
    const projects = this.http.get<Project>(urlString);
    return projects;
  }

  removeProject(projectId: string) {
    console.log("Totally removed projectId", projectId);
  }
}
