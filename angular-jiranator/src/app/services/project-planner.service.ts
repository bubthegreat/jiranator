import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

import { Project } from '../interfaces/project.interface';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private apiUrl = 'https://127.0.0.1';

  constructor(private http: HttpClient) { }

  getId(projectId: string): Observable<any> {
    const urlString = this.apiUrl + '/projects/' + projectId;
    const projectInfo = this.http.get<Project>(urlString);
    return projectInfo;
    // return {id: 1, left: leftText, right: rightText};
  }

  setId(formData) {
    const urlString = this.apiUrl + '/new-project/';
    const projectId = this.http.post(urlString, formData, httpOptions);
    return projectId;
  }
}

