import { TestBed } from '@angular/core/testing';

import { JiranatorService } from './jiranator-service.service';

describe('JiranatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JiranatorService = TestBed.get(JiranatorService);
    expect(service).toBeTruthy();
  });
});
