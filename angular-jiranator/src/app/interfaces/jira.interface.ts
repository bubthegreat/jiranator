export interface Jira {
    key: string;
    summary: string;
    priority: string;
    status: string;
}

export interface Epic {
    key: string;
    summary: string;
    name: string;
    priority: string;
    status: string;
    issues: Jira[];
    length: number;
    toDoNumber: number;
    inProgressNumber: number;
    doneNumber: number;
    otherNumber: number;
    readinessStatus: string;
    statusColor: string;
    statusReason: string;
    dueDate: string;
}
