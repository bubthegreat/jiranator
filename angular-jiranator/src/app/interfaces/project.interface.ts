
export interface Requirement {
  project: string;
  workType: string;
  storyPoints: number;
  engineeringReady: boolean;
  summary: string;
  description: string;
}

export interface Test {
  summary: string;
  description: string;
  testDate: Date;
}

export interface Project {
  project: string;
  epicName: string;
  objective: string;
  market: {
    broker: boolean;
    consultant: boolean;
    employer: boolean;
    internal: boolean;
    other: boolean;
  };
  stakeholders: string;
  opsTopTen: boolean;
  requirements: Array<Requirement>;
  testPlan: Array<Test>;
  releaseConsiderations: string;
}
