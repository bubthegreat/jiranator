"""Setup configuration and dependencies for the Crypto Logger."""

import setuptools

COMMANDS = []

PACKAGES = setuptools.find_packages()

PACKAGE_DATA = {
}

REQUIREMENTS = [requirement for requirement in open('requirements.txt').readlines()]


# pylint: disable=bad-whitespace
setuptools.setup(
    name              = 'jiranator_api',
    version           = '0.1.0.0',
    description       = 'This is a fancy async rest API!',
    packages          = PACKAGES,
    package_data      = PACKAGE_DATA,
    python_requires   = '>=3.6.6',
    entry_points      = { 'console_scripts': COMMANDS },
    install_requires  = REQUIREMENTS,
)
