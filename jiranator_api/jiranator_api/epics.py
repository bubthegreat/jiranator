"""Some jira utility functions."""

import os
import jira
import redis
import asyncio
from concurrent.futures import ThreadPoolExecutor


# Jira config
SECRET_TOKEN = os.environ.get('JIRA_SECRET_TOKEN')
JIRA_USER = os.environ.get('JIRA_SECRET_USER')
BASE_URL = os.environ.get('JIRA_SECRET_URL')
JIRA_SERVER = jira.JIRA(server=BASE_URL, basic_auth=(JIRA_USER, SECRET_TOKEN))
POOL_EXECUTOR = ThreadPoolExecutor(max_workers=10)

# Redis config
REDIS_POOL = redis.ConnectionPool(host='jiranator-redis', port=6379, db=0)
REDIS = redis.Redis(connection_pool=REDIS_POOL)


async def get_project_epics(project_key):
    """Get all epics for a project."""
    future = POOL_EXECUTOR.submit(JIRA_SERVER.search_issues, **{'jql_str': f'project = {project_key} AND issuetype = epic and status != done'})
    epics_result = await asyncio.wrap_future(future)
    return epics_result


async def get_epic_info(epic):
    """Get epic info and parse it from issues."""
    future_issues = POOL_EXECUTOR.submit(JIRA_SERVER.search_issues, **{'jql_str': f'"Epic Link" = {epic.key}'})
    issues = await asyncio.wrap_future(future_issues)
    epic_info = parse_epic_info(epic, issues)
    return epic_info


def parse_epic_info(epic, issues):
    """Pull out the epic information into a dict -> json."""
    parsed_epic = {
        'key': epic.key,
        'summary': epic.fields.summary,
        'name': epic.fields.customfield_10010,
        'priority': epic.fields.priority.name,
        'status': epic.fields.status.statusCategory.key,
        'description': epic.fields.description or "No description provided.",
        'issues': [],
        'length': len(issues),
        'toDoNumber': 0,
        'inProgressNumber': 0,
        'doneNumber': 0,
        'otherNumber': 0,
        'dueDate': epic.fields.customfield_10070
    }
    # Modify the epic in place because I'm lazy.
    set_issue_status(parsed_epic, issues)
    set_epic_status(parsed_epic, issues)
    return parsed_epic


def set_issue_status(parsed_epic, issues):
    """Update issue status for epic status column."""
    for issue in issues:
        status = issue.fields.status.statusCategory.key
        issue_json = {
            'key': issue.key,
            'summary': issue.fields.summary,
            'priority': issue.fields.priority.name,
            'status': status
        }
        if status == 'new':
            parsed_epic['toDoNumber'] += 1
        elif status == 'indeterminate':
            parsed_epic['inProgressNumber'] += 1
        elif status == 'done':
            parsed_epic['doneNumber'] += 1
        else:
            parsed_epic['otherNumber'] += 1
        parsed_epic['issues'].append(issue_json)


def set_epic_status(parsed_epic, issues):
    """Set epic status data for epic status column."""
    if len(issues) == 0:
        # No issues exist -> Not ready
        status, color, reason = 'Not Ready', 'rgb(228, 109, 109)', 'No issues exist for this epic.'
    elif parsed_epic['toDoNumber'] == parsed_epic['length']:
        status, color, reason = 'Not Started', 'gray', 'No items in the epic are in progress.'
    elif parsed_epic['doneNumber'] == parsed_epic['length']:
        status, color, reason = 'Complete', 'rgb(114, 185, 114)', 'All items in done status.'
    elif parsed_epic['inProgressNumber'] > 0 or (parsed_epic['doneNumber'] > 0 and parsed_epic['toDoNumber'] > 0):
        status, color, reason = 'In Progress', 'rgb(97, 97, 235)', 'In progress or partially complete.'
    else:
        status, color, reason = 'ERROR', 'red', 'Something went wrong!'

    parsed_epic['readinessStatus'] = status
    parsed_epic['statusColor'] = color
    parsed_epic['statusReason'] = reason


async def get_epics_and_jiras(project_key):
    """Get epic with it's jira information for the project."""
    epics = await get_project_epics(project_key)
    epic_info_list = [asyncio.ensure_future(get_epic_info(epic)) for epic in epics]
    result = await asyncio.gather(*epic_info_list)
    print(f'\n{result}\n')
    return result
