"""Project API functions and models."""

import asyncio
import hashlib
import codecs
from datetime import datetime

from typing import Dict
from typing import List
from typing import Optional


from pydantic import BaseModel

class Requirement(BaseModel):
    """Requirement class."""

    project: str
    workType: str
    storyPoints: Optional[float]
    summary: str
    description: str


class Test(BaseModel):
    """Test class."""

    summary: str
    description: str
    testDate: str


class ProjectInfo(BaseModel):
    """ProjectInfo class."""

    epicName: str
    project: str
    dueDate: str
    stakeholders: str
    objective: str
    requirements: List[Requirement] = []
    testPlan: List[Test] = []
    releaseConsiderations: str
