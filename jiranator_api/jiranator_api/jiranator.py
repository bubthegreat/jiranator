"""Basic API for jiranator."""

import asyncio
import codecs
import datetime
import hashlib
import logging
import time

import redis
import ujson
import uvicorn

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse
from starlette.responses import RedirectResponse

from jiranator_api.projects import ProjectInfo

app = FastAPI()
app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=['*'], allow_headers=['*'])
logger = logging.getLogger('fastapi')

REDIS_POOL = redis.ConnectionPool(host='localhost', port=6379, db=0)
REDIS = redis.Redis(connection_pool=REDIS_POOL)
STARTUP_TIME = datetime.datetime.now()



@app.get("/")
async def read_root():
    """Redirect to health check."""
    return RedirectResponse(url='/health')


@app.get("/health")
async def get_health():
    """Test path for making sure we're up...like a health check!"""
    uptime = str(datetime.datetime.now() - STARTUP_TIME)
    return JSONResponse({'status': 'UP', 'started': str(STARTUP_TIME), 'uptime': uptime})


@app.get("/epics-and-jiras/{project_key}")
async def get_all_for_project(project_key: str):
    """Get all the jira info for the project."""
    from jiranator_api.epics import get_epics_and_jiras
    return await get_epics_and_jiras(project_key)


@app.get("/fake-epics-and-jiras/{project_key}")
def get_all_fake_for_project(project_key: str):
    """Get all the jira fake info for the project."""
    from jiranator_api.fake_data.epics import get_fake_epics_and_jiras
    return get_fake_epics_and_jiras(project_key)


@app.post("/new-project/")
async def create_new_project(project_info: ProjectInfo):
    """Create a new project and return the id."""
    project_id = hashlib.md5(codecs.encode(project_info.json())).hexdigest()
    REDIS.set(project_id, project_info.json())
    return project_id


@app.get("/project/{project_id}")
async def get_project(project_id):
    """Return data for the project with the given id."""
    logger.info(f'Getting project for id: {project_id}')
    project_string = REDIS.get(project_id)
    return project_string

@app.get("/project/")
async def get_projects():
    """Return data for the project with the given id."""
    logger.info(f'Getting all projects.')
    project_ids = REDIS.keys()
    projects = []
    for project_id in project_ids:
        projects.append((project_id, REDIS.get(project_id)))
    return projects


@app.post("/project/{project_id}")
async def update_project(project_id, project_info: ProjectInfo):
    """Update data for the project with the given id."""
    logger.info(f'Updating project for id: {project_id}')
    REDIS.set(project_id, project_info.json())
    return True


@app.post("/create-jiras/{project_id}")
async def create_project_jiras(project_id, project_info: ProjectInfo):
    """Update data for the project with the given id."""
    logger.info(f'Creating jiras for project id: {project_id}')
    return True


def main():
    """Run through uvicorn when run."""
    uvicorn.run("jiranator:app", host='0.0.0.0', port=8000, reload=True)

if __name__ == "__main__":
    main()
