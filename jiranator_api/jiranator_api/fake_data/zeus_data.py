"""This is fake zeus data."""

fake_zeus_data = [  
  {  
    'key':'ZEUS-105',
    'summary':'A Place to log tech debt related tasks',
    'name':'Tech Debt',
    'priority':'P3',
    'status':'new',
    'description':'No description provided.',
    'issues':[  
      {  
        'key':'ZEUS-114',
        'summary':'Upgrade zeus-web play framework from version 2.5 to 2.6',
        'priority':'Medium',
        'status':'indeterminate'
      },
      {  
        'key':'ZEUS-106',
        'summary':'Angular 8 upgrade',
        'priority':'P3',
        'status':'done'
      }
    ],
    'length':2,
    'toDoNumber':0,
    'inProgressNumber':1,
    'doneNumber':1,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'In Progress',
    'statusColor':'rgb(97, 97, 235)',
    'statusReason':'In progress or partially complete.'
  },
  {  
    'key':'ZEUS-93',
    'summary':'Provider Fixes',
    'name':'Provider Fixes',
    'priority':'P3',
    'status':'new',
    'description':'No description provided.',
    'issues':[  
      {  
        'key':'ZEUS-94',
        'summary':'Provider values only getting assigned via NPI join and no coalesce',
        'priority':'Medium',
        'status':'new'
      }
    ],
    'length':1,
    'toDoNumber':1,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'Not Started',
    'statusColor':'gray',
    'statusReason':'No items in the epic are in progress.'
  },
  {  
    'key':'ZEUS-36',
    'summary':'Benchmarks',
    'name':'Benchmarks in Zeus',
    'priority':'Medium',
    'status':'new',
    'description':'Details TBD',
    'issues':[  

    ],
    'length':0,
    'toDoNumber':0,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'Not Ready',
    'statusColor':'rgb(228, 109, 109)',
    'statusReason':'No issues exist for this epic.'
  },
  {  
    'key':'ZEUS-11',
    'summary':'Transform Pipeline - Statistical Summary Preview',
    'name':'Transform Pipeline - Statistical Summary Preview',
    'priority':'Medium',
    'status':'new',
    'description':'Needs to be defined',
    'issues':[  

    ],
    'length':0,
    'toDoNumber':0,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'Not Ready',
    'statusColor':'rgb(228, 109, 109)',
    'statusReason':'No issues exist for this epic.'
  },
  {  
    'key':'ZEUS-3',
    'summary':'Data Quality Reporting Framework',
    'name':'Data Quality Reporting Framework',
    'priority':'Medium',
    'status':'new',
    'description':'Historically we have used Hestia for DQ reporting in Zeus but we eventually want to sunset it in favor of a more robust, built in solution. The purpose of this ticket is to capture the requirements of what a DQ framework needs to be able to do:\r\n\r\nGiven a database and table, be able to:\r\n* Run profiling that will report:\r\n** Column name\r\n** Data type\r\n** Fill Rate\r\n** Distinct Value Count\r\n** Top X distinct values by count\r\n** Min length\r\n** Max length\r\n** For numericals\r\n*** Mean\r\n*** Maximum\r\n*** Minumum\r\n*** Sum\r\n*** Standard Deviation\r\n* Run Checks that will report:\r\n** Fill minumum (Fail if below)\r\n** Fill target (Warn if between min and target, pass if above target)\r\n** Valid minumum (Applies to specific test, same logic as fill)\r\n** Valid target\r\n** Static categorical check - value exists in a given hard coded set of values\r\n*** Also need a negated version of this. (i.e. not in)\r\n** Reference categorical check - value exists in a given reference table\r\n** Dates\r\n*** Range\r\n*** support calculated dates (i.e. "next_year", "current_year", "sixteen_years_ago", etc)\r\n** Numerical\r\n*** less than\r\n*** greater than\r\n*** range\r\n*** non negative\r\n** Regex\r\n** String length\r\n* Checks\r\n** Need to be able to have multiple per column\r\n** Have an Artemis default but support a customer specific override',
    'issues':[  
      {  
        'key':'ZEUS-23',
        'summary':'Add ability to export a SS report to a spreadsheet',
        'priority':'Medium',
        'status':'new'
      },
      {  
        'key':'ZEUS-22',
        'summary':'Generate SS report when doing "Run Integration"',
        'priority':'Medium',
        'status':'new'
      },
      {  
        'key':'ZEUS-21',
        'summary':'Generate SS report when doing "Run Landers Only"',
        'priority':'Medium',
        'status':'new'
      }
    ],
    'length':3,
    'toDoNumber':3,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'Not Started',
    'statusColor':'gray',
    'statusReason':'No items in the epic are in progress.'
  },
  {  
    'key':'ZEUS-2',
    'summary':'Parent/Child Customers',
    'name':'Parent/Child Customers',
    'priority':'Medium',
    'status':'indeterminate',
    'description':'h3. Technical Requirements:\r\n* Add {{customer_type}} field to customer that is an enumeration of the types of customer records we deal with in Zeus. See below for possible values. This type field will allow us to turn on/off certain fields and functionality within the UI based on the value. (See table below for examples) \r\n** "Direct" - default value\r\n** "Parent" - (i.e. MMA)\r\n*** "Child" - (i.e. AMB GROUP, FLEETCOR TECHNOLOGIES, etc)\r\n* Add {{parent_id}} field to customer that allows a customer to be linked to another customer field, defaults to null. Will only be able to set this for child records and have them point at parent records.\r\n* When running a refresh\r\n** If the customer is a parent record\r\n*** If at the staging snowflake step, lookup the childrens customer keys and create a snowflake schema based on each of them in addition to the aggregate database we are currently doing. (see schema definitions below)\r\n*** If at the live snowflake step, do the same as the staging step, except, this time the user will be allowed to choose which of the child databases to push live. \r\n\r\nh3. DM Requirements:\r\n* The customer name of child record must match *EXACTLY* with the values put in the {{employer_name}} field of each of the parents integrations. This is so we can enforce referential integrity and be able to get the child customer keys to create databases with.\r\n\r\nh3. UI Requirements:\r\n* Only allow them to create a Child customer under the context of a customer that is a Parent.\r\n* When a user clicks the "Push Prod" button on a refresh for a parent customer, we will display a prompt to show them the names of all the child customers found in the refresh and allow them to select which ones should be pushed live. Default to having none of them selected the first time they see this prompt. On subsequent times they see this prompt, we can pre-fill the selections with the selections they made last time.\r\n* Child Customer Types should behave differently from Direct or Parent\r\n** Integrations cannot be associated to them\r\n** Refreshes cannot be run on them\r\n** If a record is switched from a non-child to child, and they have integrations, inactivate them. (Not sure if we are going to allow switching to a child from a non-child yet)\r\n\r\nh3. Snowflake Database & Table Requirements:\r\nWe can take advantage of [schemas|https://docs.snowflake.net/manuals/sql-reference/ddl-database.html] within snowflake as a place to put the child "databases". Given the following parent/child customers:\r\n\r\n|| Name                  || Parent/Child || customer key   ||\r\n| MMA                   | Parent       | mma            |\r\n| AMB GROUP             | Child        | amb_group      |\r\n| FLEETCOR TECHNOLOGIES | Child        | fleetcore_tech |\r\n\r\nWe would generate a snowflake database like the following:\r\n(Legend: D = Database, S = Schema, T = Table)\r\n* D: {{prod_staging_zeus_mma}}\r\n** S: {{public}} (For the parent database)\r\n*** T: {{medical_claim_line_denormalized}}\r\n*** T: {{medical_claim_line_denormalized_previous}}\r\n*** T: {{rx_claim_denormalized}}\r\n*** T: {{rx_claim_denormalized_previous}}\r\n*** etc...\r\n** S: {{amb_group}}\r\n*** T: {{medical_claim_line_denormalized}}\r\n*** T: {{medical_claim_line_denormalized_previous}}\r\n*** etc...\r\n** S: {{fleetcore_tech}}\r\n*** etc...',
    'issues':[  
      {  
        'key':'ZEUS-122',
        'summary':'Snowflake Benchmarks Table',
        'priority':'Medium',
        'status':'new'
      },
      {  
        'key':'ZEUS-113',
        'summary':'Add coalesce Integration V1 V2 Counts to Customers Table',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'ZEUS-79',
        'summary':'Validate parent/child setup mid-refresh',
        'priority':'Medium',
        'status':'new'
      },
      {  
        'key':'ZEUS-78',
        'summary':'Display Child Records in New Refresh Screen',
        'priority':'Medium',
        'status':'done'
      },
      {  
        'key':'ZEUS-77',
        'summary':'Parent Child Ph2',
        'priority':'Medium',
        'status':'done'
      },
      {  
        'key':'ZEUS-56',
        'summary':'Parent/Child Snowflake schemas',
        'priority':'Medium',
        'status':'indeterminate'
      },
      {  
        'key':'ZEUS-53',
        'summary':'Create Atomic Snowflake Databases',
        'priority':'P3',
        'status':'indeterminate'
      },
      {  
        'key':'ZEUS-45',
        'summary':'Display/Edit Child Records',
        'priority':'Medium',
        'status':'done'
      },
      {  
        'key':'ZEUS-44',
        'summary':'Creation of child customer records',
        'priority':'Medium',
        'status':'done'
      },
      {  
        'key':'ZEUS-13',
        'summary':'Add parent_id to customer',
        'priority':'Medium',
        'status':'done'
      },
      {  
        'key':'ZEUS-12',
        'summary':'Create POC Snowflake database with multiple schemas',
        'priority':'Medium',
        'status':'done'
      }
    ],
    'length':11,
    'toDoNumber':2,
    'inProgressNumber':2,
    'doneNumber':7,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'In Progress',
    'statusColor':'rgb(97, 97, 235)',
    'statusReason':'In progress or partially complete.'
  }
]