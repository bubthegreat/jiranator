
from jiranator_api.fake_data.pp_data import fake_pp_data
from jiranator_api.fake_data.zeus_data import fake_zeus_data

def get_fake_epics_and_jiras(project_key):
    """Return faked data"""
    if project_key == 'PP':
        return fake_pp_data
    elif project_key == 'ZEUS':
        return fake_zeus_data
    else:
        return []