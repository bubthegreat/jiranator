fake_pp_data = [  
  {  
    'key':'PP-751',
    'summary':'Multiple SSO Companies',
    'name':'Mercer/Boeing SSO',
    'priority':'P2',
    'status':'new',
    'description':'We need a way for a consultant to log in to multiple SSO companies. Currently, a customer can only log in using SSO with their default company. \r\n\r\nIn Hermes we need to iterate over all company users to see if any company offers SSO. If not or there is only one IDP defined then behave as the site does currently. If multiple are found we need to return a list of IDP links and company names:\r\n\r\n\r\n{code:JSON}\r\n[\r\n    {\r\n        "name": "Stark Industries",\r\n        "idp_url": "https://idp.server.com/saml"\r\n    },\r\n    {\r\n        "name": "Gringotts",\r\n        "idp_url": "http://idp.different-server.com/sso"\r\n    }\r\n]\r\n{code}\r\n\r\n\r\n\r\nOn the frontend, we will need for a page created to present the customer with a list of companies to log in to using their IDP servers in the JSON payload. ',
    'issues':[  
      {  
        'key':'PP-755',
        'summary':'Consultants Need to Show in Their Shell Companies',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-754',
        'summary':'Multiple SSO Company Selection Page Design',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-753',
        'summary':'Multiple SSO Companies API',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-752',
        'summary':'Multiple SSO Company Selection Page Implementation',
        'priority':'P3',
        'status':'new'
      }
    ],
    'length':4,
    'toDoNumber':4,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':'2019-06-19',
    'readinessStatus':'Not Started',
    'statusColor':'gray',
    'statusReason':'No items in the epic are in progress.'
  },
  {  
    'key':'PP-746',
    'summary':'Claims Lag Report - pushed to UI for external client use (also include a filter function)',
    'name':'Claims Lag App',
    'priority':'High',
    'status':'new',
    'description':'Please push the claims lag reporting app to the UI for external client users. This is something both our employer and broker clients have been asking for and it looks easy enough for them to use on their own. \r\n\r\nAs an experienced Verscend and Truven user, I can say that us not having an easy way for users to create clean lag reports on their own is a significant deficiency. All of our major competitors have this "baked" in. Our version looks clean and easy, and I am certain that making it available externally would be appreciated.\r\n\r\nThe only thing I would ask is that the ability to filter is added. Clients will want to limit the results to a variety of different populations, plans, regions, etc. ',
    'issues':[  

    ],
    'length':0,
    'toDoNumber':0,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'Not Ready',
    'statusColor':'rgb(228, 109, 109)',
    'statusReason':'No issues exist for this epic.'
  },
  {  
    'key':'PP-741',
    'summary':'Gaps In Care: Frontend experience and work',
    'name':'Gaps In Care',
    'priority':'P3',
    'status':'new',
    'description':'No description provided.',
    'issues':[  

    ],
    'length':0,
    'toDoNumber':0,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':'2019-07-31',
    'readinessStatus':'Not Ready',
    'statusColor':'rgb(228, 109, 109)',
    'statusReason':'No issues exist for this epic.'
  },
  {  
    'key':'PP-729',
    'summary':'Levithan Google Security Penetration Test Findings',
    'name':'Levithan Google Security Bugs',
    'priority':'P1',
    'status':'new',
    'description':'Google external Levithan Auditors identified some critical, low and informational bugs in our system',
    'issues':[  
      {  
        'key':'PP-738',
        'summary':'POSSIBLE SQL "INJECTION" BY MANIPULATING PREPARED STATEMENTS',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-737',
        'summary':'SPIKE: OPEN HTTP REDIRECT ON /AUTH/LOGIN',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-736',
        'summary':'S3 BUCKET WORLD READABLE',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-735',
        'summary':'CONTENT SECURITY POLICY IS NOT SET ',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-733',
        'summary':'Review how we identify superusers between frontend and backend',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-732',
        'summary':'ACCOUNT LOCKOUT CAN BE ABUSED TO DISCOVER USER PASSWORDS',
        'priority':'P4',
        'status':'new'
      },
      {  
        'key':'PP-731',
        'summary':'You can PATCH/PUT to /api/users to escalate your position, modify your email and take over accounts',
        'priority':'P1',
        'status':'done'
      },
      {  
        'key':'PP-730',
        'summary':'You can PATCH roles and permissions on the api/permissions endpoint without proper authorization',
        'priority':'Critical',
        'status':'done'
      }
    ],
    'length':8,
    'toDoNumber':5,
    'inProgressNumber':0,
    'doneNumber':3,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'In Progress',
    'statusColor':'rgb(97, 97, 235)',
    'statusReason':'In progress or partially complete.'
  },
  {  
    'key':'PP-665',
    'summary':'A collection of simple changes to improve the experience.',
    'name':'Job Jar Q2',
    'priority':'P3',
    'status':'new',
    'description':'h1. [Link to InVision ↗|http://bit.ly/jobjar]',
    'issues':[  
      {  
        'key':'PP-674',
        'summary':'Prevent measure/breakdown chooser from closing when click originates inside search input',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-673',
        'summary':'Indicate hidden options available for measures and breakdowns in Visualize',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-667',
        'summary':'Fix company menu selection indicator and treat sub-companies the same way',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-666',
        'summary':'Link Artemis logo to home in new nav',
        'priority':'Low',
        'status':'done'
      }
    ],
    'length':4,
    'toDoNumber':3,
    'inProgressNumber':0,
    'doneNumber':1,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'In Progress',
    'statusColor':'rgb(97, 97, 235)',
    'statusReason':'In progress or partially complete.'
  },
  {  
    'key':'PP-654',
    'summary':'Chronos 2.0: Work for Chronos 2.0 to be implemented in production (backend)',
    'name':'Chronos 2.0 Backend',
    'priority':'P3',
    'status':'new',
    'description':'work description for backend items that need to be implemented for chronos 2.0',
    'issues':[  

    ],
    'length':0,
    'toDoNumber':0,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'Not Ready',
    'statusColor':'rgb(228, 109, 109)',
    'statusReason':'No issues exist for this epic.'
  },
  {  
    'key':'PP-653',
    'summary':'Chronos 2.0: Work for Chronos 2.0 to be implemented in production (frontend)',
    'name':'Chronos 2.0 Frontend',
    'priority':'P3',
    'status':'new',
    'description':"As we scale up we need to improve the framework we're using for the frontend.  This is to scope out the frontend work for the Chronos 2.0 infrastructure and the work associated with it.",
    'issues':[  
      {  
        'key':'PP-762',
        'summary':'Finish initial setup and config of artemis library',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-761',
        'summary':'Finish tests for artemis library (scoped to metric chooser)',
        'priority':'P3',
        'status':'indeterminate'
      },
      {  
        'key':'PP-760',
        'summary':'Port over metric chooser from Chronos to Artemis Library',
        'priority':'P3',
        'status':'indeterminate'
      },
      {  
        'key':'PP-759',
        'summary':'Finish building NgRx Store for Artemis Library',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-758',
        'summary':'Artemis Library - Jenkins file - autorun tests and publish library',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-749',
        'summary':'Integrate artemis-library into Artemis App for Measure chooser and card',
        'priority':'P3',
        'status':'indeterminate'
      },
      {  
        'key':'PP-748',
        'summary':'Integrate artemis-library into Chronos for Measure chooser and card',
        'priority':'P3',
        'status':'indeterminate'
      }
    ],
    'length':7,
    'toDoNumber':1,
    'inProgressNumber':4,
    'doneNumber':2,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'In Progress',
    'statusColor':'rgb(97, 97, 235)',
    'statusReason':'In progress or partially complete.'
  },
  {  
    'key':'PP-614',
    'summary':'UHC Provider Compliance',
    'name':'UHC Provider Compliance',
    'priority':'P3',
    'status':'new',
    'description':'UHC will be requiring us not to show combinations of measures, breakdowns, and filters that allow users to identify the discounts providers give to the carrier.  In order to be compliant with UHC, we\'ll need to redact values and make it clear to customers that UHC is forcing this. In explore, visualize, and stories, any values with a combination of breakdowns or filters using Provider Name, Provider Street Address, Provider NPI, and Provider TIN AND measures associated with a category of cost should state as the value "redacted by UHC".  In this scenario, if a customer had UHC data AND any other provider, only the UHC values would be redacted.  ',
    'issues':[  
      {  
        'key':'PP-830',
        'summary':'Need testing for - cannot use filters related to those for those breakdowns',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-829',
        'summary':'Need testing for redaction showing in breakdowns',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-828',
        'summary':'Need flag to identify permission on the backend for subcompany',
        'priority':'P3',
        'status':'indeterminate'
      },
      {  
        'key':'PP-763',
        'summary':'Breakdown values of prohibited breakdowns will indicate redaction per UHC',
        'priority':'P3',
        'status':'indeterminate'
      },
      {  
        'key':'PP-624',
        'summary':'Need to investigate Standard Stories to see whether the blacklisting affects any of them',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-621',
        'summary':'Need to wholesale prevent filters on breakdowns for blacklist',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-619',
        'summary':'Analytic Advisor would have to control turning it off. Should default to on.',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PM-52',
        'summary':'Charts will display redacted text in place of breakdown values',
        'priority':'P3',
        'status':'done'
      }
    ],
    'length':8,
    'toDoNumber':5,
    'inProgressNumber':2,
    'doneNumber':1,
    'otherNumber':0,
    'dueDate':'2019-06-21',
    'readinessStatus':'In Progress',
    'statusColor':'rgb(97, 97, 235)',
    'statusReason':'In progress or partially complete.'
  },
  {  
    'key':'PP-603',
    'summary':'Dynamic Cohorts',
    'name':'Dynamic Cohorts',
    'priority':'P3',
    'status':'new',
    'description':'A dynamic Cohort is a selected filter and then putting people in or out of that filter, calculated at runtime. ',
    'issues':[  
      {  
        'key':'PP-609',
        'summary':'Dynamic Cohorts can be saved as a custom breakdown ',
        'priority':'P3',
        'status':'new'
      }
    ],
    'length':1,
    'toDoNumber':1,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':'2019-08-31',
    'readinessStatus':'Not Started',
    'statusColor':'gray',
    'statusReason':'No items in the epic are in progress.'
  },
  {  
    'key':'PP-592',
    'summary':'Physician Scores & Hospital Scores',
    'name':'HEB Provider Quality',
    'priority':'P3',
    'status':'new',
    'description':'No description provided.',
    'issues':[  
      {  
        'key':'PP-612',
        'summary':'HEB: Navlink enhancements in physician scores',
        'priority':'Low',
        'status':'done'
      },
      {  
        'key':'PP-593',
        'summary':'MSA missing',
        'priority':'P3',
        'status':'done'
      }
    ],
    'length':2,
    'toDoNumber':0,
    'inProgressNumber':0,
    'doneNumber':2,
    'otherNumber':0,
    'dueDate':'2019-06-19',
    'readinessStatus':'Complete',
    'statusColor':'rgb(114, 185, 114)',
    'statusReason':'All items in done status.'
  },
  {  
    'key':'PP-572',
    'summary':'Enhancements and Support for Custom Measures ',
    'name':'Custom Measures Enhancements',
    'priority':'P3',
    'status':'new',
    'description':'No description provided.',
    'issues':[  

    ],
    'length':0,
    'toDoNumber':0,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'Not Ready',
    'statusColor':'rgb(228, 109, 109)',
    'statusReason':'No issues exist for this epic.'
  },
  {  
    'key':'PP-421',
    'summary':'Ability to treat Sub-companies as their own Company',
    'name':'Company (de)aggregation',
    'priority':'P3',
    'status':'new',
    'description':'No description provided.',
    'issues':[  
      {  
        'key':'PP-824',
        'summary':"As a child company I want to compare my population to the book of business I'm in",
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-823',
        'summary':'We need to know what the query looks like to get benchmarks requests for sub companies ',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-822',
        'summary':'Company needs to be in place of sub company everywhere like in the admin and in the dropdown navigation',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-821',
        'summary':"make sure roles and permissions don't break with the new subcompanies",
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-820',
        'summary':'need to link the new sub companies to their current metrics in airtable ',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-819',
        'summary':'need to recreate all currently live sub companies in airtable',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-818',
        'summary':'Airtable needs to have the new parent company id ',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-817',
        'summary':'remove current sub company from session',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-816',
        'summary':'make sure that adding sub companies to users in the Artemis admin still works ',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-815',
        'summary':'update the current user end point to point to new companies instead of subcompanies',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-814',
        'summary':'remove all branch code for subcompanies',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-813',
        'summary':'make sure benchmark requests work',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-812',
        'summary':'ensure metric sync works',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-811',
        'summary':'Need a data migration',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-810',
        'summary':'Need to do a schema migration ',
        'priority':'P3',
        'status':'new'
      }
    ],
    'length':15,
    'toDoNumber':15,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':'2019-07-31',
    'readinessStatus':'Not Started',
    'statusColor':'gray',
    'statusReason':'No items in the epic are in progress.'
  },
  {  
    'key':'PP-359',
    'summary':'Benchmarks for V2 (Right after Elevate)',
    'name':'Benchmarks V2',
    'priority':'P3',
    'status':'new',
    'description':'No description provided.',
    'issues':[  
      {  
        'key':'PP-660',
        'summary':'Ensure that selected service category is shown when moving from Trend Explorer --> Explore',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-659',
        'summary':'Graceful failure when there is a background or browser issue',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-658',
        'summary':'Clarify which column is sorted',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-657',
        'summary':'Link directly to explanations of loosely, moderately, and well managed',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-629',
        'summary':'Prescriptions are missing from the dropdown list inside the service category level 3 filter chooser',
        'priority':'Medium',
        'status':'done'
      },
      {  
        'key':'PP-611',
        'summary':'Link top 5 impactable trends to Explore in Trend Explorer',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-597',
        'summary':'Permissions - With Milliman Benchmarks Permission Turned Off The Benchmarks Dropdown Is Still Available in Standard Stories',
        'priority':'Critical',
        'status':'done'
      },
      {  
        'key':'PP-589',
        'summary':'Adding Trend Charts To Explorations Lots of Overlap',
        'priority':'Medium',
        'status':'done'
      },
      {  
        'key':'PP-586',
        'summary':'As a user coming back to Trend Explorer after clicking into Explore, I can see all the Setup and Filter changes I made before',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-570',
        'summary':'Explorations - Sparkline Gets Pushed Down When Filter Applied',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-564',
        'summary':'Make trend explorer med/rx calculation based on separate values rather than rollup',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-561',
        'summary':'Visualize - Adding More Measures Not Working',
        'priority':'P1',
        'status':'done'
      },
      {  
        'key':'PP-557',
        'summary':'Visualizations - Benchmarks Warning Displayed When Nothing Regarding Benchmarks Was Chosen',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-556',
        'summary':'Trend explore hyperlink defaults to CCS level 2 breakdown and not HCG service level 3',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-555',
        'summary':'Add percent change arrow to trend drivers card for percentage change to match elsewhere',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-550',
        'summary':'Trend Explorer: "Top Impactable Trend Drivers" Card > add "v Util Benchmark" column so all three components of the selection criteria are visible (and since we have more room now)',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-549',
        'summary':'Add links to service category in "top impactable trend drivers" card https://artemis-health.slack.com/archives/CG8AN4ENP/p1556381331131100',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-548',
        'summary':'Trend Explorer - When at the bottom of the first page, and then go to next page you remain at the bottom',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-547',
        'summary':'Trend Explorer - Utilization Trend for Preventive Care showing an increase in red. These are good things so they should be green',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-546',
        'summary':'Visualizations - Have the ability to pull the benchmark value into a value card visualization in a story',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-541',
        'summary':'Measure Screen - Custom Measures and Milliman. If you have a lot of custom measures, there is no scroll bar in the benchmark dropdown. MMA is prime example.',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-539',
        'summary':'Custom Benchmarks Titles Have A Length of 150 Characters. If you use all 150 characters The benchmark values are pushed completely out of the grey bar in the exploration. ',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-533',
        'summary':'Exploration - Benchmark on the Sparkline missing',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-532',
        'summary':'Benchmarks are off screen with certain comparisons',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-527',
        'summary':"Export. to CSV doesn't show benchmark",
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-525',
        'summary':'Standard Stories: Executive Summary & Pharmacy default benchmarks to moderately managed on page load',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-523',
        'summary':'Explore App: If you add a measure, then add a benchmark, then go into the measure chooser and select another measure, the dark grey box from the previous benchmark remains on the screen',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-522',
        'summary':'Safari: Chart in trend explorer is grey and not yellow, red and green',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-521',
        'summary':'Measure picker: Count of measures should drop to actual number in the list when hiding measures without benchmarks',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-520',
        'summary':'[Lower priority] Trend explorer: add solid line to the side of the sticky column',
        'priority':'P4',
        'status':'done'
      },
      {  
        'key':'PP-519',
        'summary':'[Not release blocker ] Trend explorer: If you select bubbles on the chart, then add a filter from either of the other two places, the chart redraws and the selected bubbles are no longer blue',
        'priority':'P4',
        'status':'done'
      },
      {  
        'key':'PP-515',
        'summary':'Explore: I get 3 loading box "spinners" in one explore summary card when loading new data',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-514',
        'summary':"Export CSV date range doesn't match the date range selected in app (Scott doesn't think benchmarks V1 release blocker)",
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-513',
        'summary':'If you modify two benchmarks in quick succession, the "autosave" boots out my changes to one of the two items and only keeps one.  ',
        'priority':'P4',
        'status':'done'
      },
      {  
        'key':'PP-510',
        'summary':'Changing anything the loading of the trend explorer is very slow',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-507',
        'summary':'In explore, benchmarks with a long custom name have text that spills over as it wraps',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-499',
        'summary':'Standard Stories - Benchmark Not Showing In Downloaded PDF',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-485',
        'summary':'IE 11 positive benchmarks are showing an up and down arrow:',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-484',
        'summary':'IE11 performance. When clicking the ellipsis on a measure to get the drop down it is a good 5 seconds or more before the dropdown appears. ',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-482',
        'summary':'The loading time between switching from the custom benchmarks app to other areas of the application is too long. It is even worse in IE 11',
        'priority':'P2',
        'status':'done'
      },
      {  
        'key':'PP-480',
        'summary':'IE 11 Standard Stories loading icons still spinning after everything has loaded: ',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-479',
        'summary':'IE11 Exploration with Milliman and Custom benchmark The custom benchmark is showing n/a even though the milliman benchmark is chosen. When benchmarks are chosen the loading spinner just stays and never goes away.',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-478',
        'summary':'IE11 Exploration still showing loading spinner in after several minutes',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-475',
        'summary':'Benchmark label is under the line chart: ',
        'priority':'Medium',
        'status':'done'
      },
      {  
        'key':'PP-470',
        'summary':"looks like the benchmark color is using opacity which makes it look different when there's a bar behind. Can we pick a solid color that will look the same on or off the bar?",
        'priority':'P4',
        'status':'done'
      },
      {  
        'key':'PP-455',
        'summary':'Validate the "can\'t apply custom benchmarks" list for frontend',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-451',
        'summary':'Users should be able to export CSV with benchmark information',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-397',
        'summary':'Benchmarks V1: Benchmarks should be available in the header of the PDF download',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-382',
        'summary':'Benchmarks V2: Apps using benchmarks should not run noticeably slower (add > 1 second) than running without benchmarks',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-358',
        'summary':'Benchmarks: Support Breakdowns',
        'priority':'P3',
        'status':'new'
      }
    ],
    'length':50,
    'toDoNumber':4,
    'inProgressNumber':0,
    'doneNumber':46,
    'otherNumber':0,
    'dueDate':'2019-06-30',
    'readinessStatus':'In Progress',
    'statusColor':'rgb(97, 97, 235)',
    'statusReason':'In progress or partially complete.'
  },
  {  
    'key':'PP-313',
    'summary':'Rework Actionable Overspending Book Of Business to a maintainable state',
    'name':'Artemis BoB Benchmarks',
    'priority':'P3',
    'status':'new',
    'description':'Actionable Overspending Book Of Business needs to be coded in a way that the benchmarks can be updated',
    'issues':[  
      {  
        'key':'PP-334',
        'summary':'Artemis Bob benchmarks back end work for calculating and managing values',
        'priority':'P2',
        'status':'new'
      },
      {  
        'key':'PP-333',
        'summary':'Artemis BoB benchmarks front end work for managing values',
        'priority':'P2',
        'status':'new'
      }
    ],
    'length':2,
    'toDoNumber':2,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':'2019-07-31',
    'readinessStatus':'Not Started',
    'statusColor':'gray',
    'statusReason':'No items in the epic are in progress.'
  },
  {  
    'key':'PP-301',
    'summary':'Remove Medical Claims Table',
    'name':'Remove Medical Claims Table',
    'priority':'P4',
    'status':'new',
    'description':"See what the consequences are for removing the claims table and applying Dallin's fix for in measure filters\r\n\r\nhttps://filter.artemishealth.com/app/\r\n\r\nhttps://artemishealth.atlassian.net/browse/PP-278\r\nhttps://artemishealth.atlassian.net/browse/PP-279\r\nhttps://artemishealth.atlassian.net/browse/PP-293",
    'issues':[  
      {  
        'key':'UX-8',
        'summary':'Validation Plan',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-341',
        'summary':'Remove all the med app based permissions',
        'priority':'Low',
        'status':'done'
      },
      {  
        'key':'PP-332',
        'summary':'Remember to remove med link from chronos',
        'priority':'Medium',
        'status':'done'
      },
      {  
        'key':'PP-283',
        'summary':'Medical Claim App appears to be duplicated records',
        'priority':'P2',
        'status':'done'
      }
    ],
    'length':4,
    'toDoNumber':1,
    'inProgressNumber':0,
    'doneNumber':3,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'In Progress',
    'statusColor':'rgb(97, 97, 235)',
    'statusReason':'In progress or partially complete.'
  },
  {  
    'key':'PP-273',
    'summary':'Support having clauses in the app',
    'name':'Having Clause ',
    'priority':'P3',
    'status':'new',
    'description':'h1. Matt Born is actively working on this description\r\n\r\nTODO',
    'issues':[  

    ],
    'length':0,
    'toDoNumber':0,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':None,
    'readinessStatus':'Not Ready',
    'statusColor':'rgb(228, 109, 109)',
    'statusReason':'No issues exist for this epic.'
  },
  {  
    'key':'PP-240',
    'summary':'Generate 1,000+ reports for clients at one time',
    'name':'Kentuckiana',
    'priority':'P4',
    'status':'indeterminate',
    'description':'The Kentuckiana Health Collaborative (KHC) has contracted with us to generate reports to provide to all individual and group practices in the Kentuckiana region (7 counties) and the broader state of Kentucky.\r\n',
    'issues':[  
      {  
        'key':'PP-826',
        'summary':'Add labels to slider for 0 and 100',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-825',
        'summary':'Change bottom 10% on slider to bottom 25% and adjust the size of the slider accordingly',
        'priority':'P3',
        'status':'indeterminate'
      },
      {  
        'key':'PP-769',
        'summary':'Put in blanks and hyphens in the Medicaid/Commercial Columns instead of 0 and 0% in the cases where they do not have Medicaid/Commercial data',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-768',
        'summary':'Change “Percent above/below average” to “Measures above/below average” on the 2nd and 3rd cards',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-767',
        'summary':'Remove the previous year column on the "your score compared to" section of the individual report',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-766',
        'summary':'Add NPI under the provider name on the individual report',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-636',
        'summary':'the two overview boxes are the same template as the left and right overview boxes on the group report.',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-635',
        'summary':'the bottom paragraph matches May 13 mockup.',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-634',
        'summary':'each table row contains pill bar on top of 0-100 line with “bottom 10%“ and “top 25% benchmark” value displayed on each end with “your practice” plotted between and a legend in table head above with labels.',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-633',
        'summary':'the right overview box shows bottom 2 measures by percent below average with percent and measure name.',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-632',
        'summary':'the center overview box shows top 3 measures by percent above average with percent and measure name.',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-631',
        'summary':'the left overview box shows total count of measures on report, number above average, and number performing better than benchmark with Benchmarks Provider ribbon icon.',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-630',
        'summary':'the top paragraph matches May 13 mockup.',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-241',
        'summary':'Kentuckiana PDF reports',
        'priority':'P2',
        'status':'indeterminate'
      }
    ],
    'length':14,
    'toDoNumber':0,
    'inProgressNumber':2,
    'doneNumber':12,
    'otherNumber':0,
    'dueDate':'2019-05-31',
    'readinessStatus':'In Progress',
    'statusColor':'rgb(97, 97, 235)',
    'statusReason':'In progress or partially complete.'
  },
  {  
    'key':'PP-24',
    'summary':'Single Sign On with Google',
    'name':'Google SSO',
    'priority':'P3',
    'status':'indeterminate',
    'description':'*User Goal*\r\nAs a admin, I want to set up SSO using Google so that my employees don’t have credentials across any number of services and our IT security team can maintain authentication compliance\r\n\r\n*Explanation*\r\n* Large enterprises and highly regulated industries often have unique security requirements and want their own independent identity solution.  This increases security by hosting authentication data in one place and the interesting data in another.  Additionally, it makes life easier for users as they only have to remember or use one set of authentication credentials. \r\n\r\n* Our competitors do not have single sign on capabilities so this will be a small competitive differentiator in selling to large enterprises.   Currently, we are aware of 2 customers (Fidelity, Huntington Bank) who are aggressively requesting this but likely others will be interested once we get it up and running.  We would anticipate roughly 10-15% of our customer base using this capability.  \r\n\r\n* It largely includes two major pieces:  setup of Google by the admin, then the activity of logging in using Google.  The admin will still be able to create and leverage standalone Artemis identities so that consultants and Artemis analytic advisors can log in and provide assistance and insights. \r\n\r\n* While the scope of this initial story does not include auto-provisioning users or automatic authentication to the OS login credentials, in the future we will likely incorporate those capabilities.  Additionally, we will likely add LDAP and AD SSO in the future.  The capability includes the ability to make the feature unavailable per customer - we will incorporate pricing levels at some point in the future and SSO is a prime candidate for enterprise-only pricing.\r\n\r\n*Acceptance Criteria*\r\n* Admins should be able to enable/disable SSO at the company level\r\n* Admins should be able to select Google in the setup area as a global setting (default to Google)\r\n* Google authenticated users can co-exist with Artemis authenticated users\r\n* Any user in the system should be able to be configured to either use Google or Artemis credentials\r\n* Artemis will honor session logout times set by the Google IDP if set\r\n* SSO can be turned off per customer by Artemis based on a pricing package\r\n\r\n*Admin Setup Acceptance Criteria* \r\n* Admin area should allow admins to enter data required to authenticate Google users\r\n* Binding Type (Redirect, Post, SOAP, or Artifact) - required (defaults to Redirect)\r\n* Identity Provider Metadata XML - required\r\n* Log-In Portal URL - required\r\n* Sign-Out Portal URL\r\n* Forgot/Change Password URL\r\n* SSO Error Page URL\r\n* Secure Hash (SHA-1, SHA-2, SHA-256, or SHA-512) - required (defaults to SHA-256)\r\n* Upload Certificate - required\r\n* Admin should be able to test the connection from the setup area when all required fields are filled out - test should include testing URLs to make sure they are valid and that the SSO connection is successful\r\n* Successful test = test passed\r\n* Failed test = failed test + reason test failed\r\n* Artemis should provide key information for the admin to provide to the identity provider\r\n* Artemis Google Metadata XML File and a link to the XML metadata\r\n* Artemis identity provider URL\r\n* Custom link for autoredirect to SSO (skips login page)\r\n* Assertion Consumer Service URL\r\n* Artemis should allow the IDP to provide metadata about individual users first name, last name, e-mail address, and default role based on a mapping of fields (if this requirement is onerous, we can discuss deferring it)\r\n* Admin should be able to identify a newly created user as a SSO user (how do we map an admin created user to their SSO authentication)\r\n*Login Acceptance Criteria*\r\n* If single sign on is enabled for the company we recognize the user and redirect them to the SSO app as configured in the admin setup\r\n* Users logging in with SSO should not have to redirect themselves to the SSO page after their first login\r\n* Assuming that SSO has been enabled for a company, if a user attempts to log in without having been set up in the Artemis system, that user should be able to log in successfully with minimum security* (under review)\r\n* If an error occurs during log in, the user should be redirected to the SSO Error Page URL.  If the admin has not configured a SSO Error Page, the user should be redirected to the Artemis login page with an explanation that the login was unsuccessful and they should contact their SSO administrator if the error persists\r\n* If the admin has set up a “Forgot/Change Password URL”, a link should be available to navigate to that URL to reset or change their password\r\n* Non-SSO users should be able to skip the “username only” screen after their first login attempt and not hit it again if they log in more often than monthly \r\n* If a user is initially created as a Non-SSO user, sets up a password, and logs in, then subsequently is converted to SSO, they should be treated as an SSO user\r\n* Log Out\r\n* When a user logs out their IDP session should be ended\r\n* If the admin has set up a sign-out portal, when a user logs out they should be redirected to the configured sign-out portal\r\n\r\n*Performance Expectations*\r\n* Identify the performance expectations for the capability so that the user has a great experience\r\n* Upon selecting Google, page should be ready for data entry in less than 1 second\r\n* Testing for credential should take less than 10 seconds and make clear what activity is occurring during the test\r\n* Upload overhead time should be less than 3 seconds and upload time should be proportional to the size of the upload file\r\n* Redirects to external URLs should occur within 3 seconds\r\n\r\n',
    'issues':[  
      {  
        'key':'PP-765',
        'summary':'Google SSO: Unhide login button on home page',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-764',
        'summary':'Hide google SSO button from prod for now',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-715',
        'summary':'Google SSO: Create end to end tests',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-714',
        'summary':'Google SSO: Caching/Cookie',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-713',
        'summary':'Google SSO: Configurations should be separate for the various environments',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-712',
        'summary':"Google SSO: Redirect to sign in page when login doesn't exist for google SSO",
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-711',
        'summary':'Google SSO: Sign in button on front page',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-710',
        'summary':'Google SSO: Sign out portal redirect if configured',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-708',
        'summary':'Google SSO: Ability to logout',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-707',
        'summary':'Google SSO: Convert normal users to SSO users',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-706',
        'summary':'Google SSO: Skip username only screen for non-sso users',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-705',
        'summary':'Google SSO: Forgot/Change password URL link when configured',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-704',
        'summary':'Google SSO: SSO Error page redirect on error',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-702',
        'summary':'Google SSO: No redirect to SSO after first login',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-701',
        'summary':'Google SSO: Redirect to SSO as configured if enabled',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-700',
        'summary':'Google SSO: Identify users as SSO during after creation; As an admin, I should be able to toggle an on/off button for SSO for my company',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-698',
        'summary':'Google SSO: Assertion Consumer Service URL',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-697',
        'summary':'Google SSO: Auto-redirect SSO custom link configuration',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-696',
        'summary':'Google SSO: Artemis identity provider URL',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-695',
        'summary':'Google SSO: Metadata XML file and link to XML metadata available in admin',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-694',
        'summary':'Google SSO: Provide key information for SSO admin',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-693',
        'summary':'Google SSO: Give a failure message when the test failson the admin page',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-692',
        'summary':'Google SSO: Give a success message when the test succeeds on the admin page',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-691',
        'summary':'Google SSO: Admin should be able to test the connection from the setup area when all required fields are filled out - test should include testing URLs to make sure they are valid and that the SSO connection is successful',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-690',
        'summary':'Google SSO: Upload Certificate - required',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-689',
        'summary':'Google SSO: Secure Hash (SHA-1, SHA-2, SHA-256, or SHA-512) - required (defaults to SHA-256)',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-688',
        'summary':'Google SSO: SSO Error Page URL',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-687',
        'summary':'Google SSO: Forgot/Change Password URL',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-686',
        'summary':'Google SSO: Sign-Out Portal URL',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-685',
        'summary':'Google SSO: Log-In Portal URL',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-684',
        'summary':'Google SSO: Identity Provider Metadata XML - required',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-683',
        'summary':'Google SSO: Binding Type (Redirect, Post, SOAP, or Artifact) - required (defaults to Redirect)',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-682',
        'summary':'Google SSO: Admin area should allow admins to enter data required to authenticate Google users',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-681',
        'summary':"As an artemis employee, I can remove the SSO capability, and if it's disabled, this functionality won't show up anywhere.",
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-679',
        'summary':'As a user, it should not matter if my account was created manually or I used my google SSO account - they should inherit the same functionality from configuration at the company level, etc.',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-678',
        'summary':'As an admin, if I go to the setup area, I can select google as a global setting.  This would be the default if I navigate there.',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-677',
        'summary':'As an admin, I should be able to toggle an on/off button for SSO for my company',
        'priority':'P3',
        'status':'done'
      },
      {  
        'key':'PP-201',
        'summary':'[Spec] Single Sign On with Google',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-76',
        'summary':'SPIKE: "Test SSO" Endpoint',
        'priority':'P3',
        'status':'new'
      }
    ],
    'length':39,
    'toDoNumber':14,
    'inProgressNumber':0,
    'doneNumber':25,
    'otherNumber':0,
    'dueDate':'2019-06-30',
    'readinessStatus':'In Progress',
    'statusColor':'rgb(97, 97, 235)',
    'statusReason':'In progress or partially complete.'
  },
  {  
    'key':'PP-12',
    'summary':'Program Stories in app',
    'name':'Program Stories',
    'priority':'P3',
    'status':'new',
    'description':"As a user I want to know, is my benefits program working, so that I can justify future programs and understand if the program I implemented is having the impact I thought it would.\r\n\r\n*Program 1*\r\nType: Onsite Clinic\r\nGoal: Cost Savings\r\nMeasures: \r\n* Count Program Participation -- Eligible\r\n* Count Program Participation -- Enrolled\r\n* Count Program Participation -- Engaged\r\n* Count Program Participation -- Completed\r\n* Identified Candidates\r\n* Percent Participation\r\n* On Site Primary Care Physician Visits (Secondary)\r\n* On Site Specialist Visits (Secondary)\r\n* Community Primary Care Physician Visits (Secondary)\r\n* Community Specialists Visits (Secondary)\r\n* Med, Rx Total Allowed (Primary)\r\n* Med, Rx Per Member Per Month (Primary)\r\n* Short Term Disability Cost per Employee (Primary)\r\n* Workers' Compensation Cost per Employee (Primary)\r\n\r\n----\r\n\r\n*Program 2*\r\nType: Diabetes Management\r\nGoal: Increase Quality of Life\r\nMeasures: \r\n* Count Program Participation -- Eligible\r\n* Count Program Participation -- Enrolled\r\n* Count Program Participation -- Engaged\r\n* Count Program Participation -- Completed\r\n* Identified Candidates\r\n* Percent Participation\r\n* Gaps in Care - Annual Physical, Annual A1c, LDL-C, Annual Foot Exam, Annual Eye Exam (Primary)\r\n* Glucose Level (Primary)\r\n* Emergency Room Visits (Secondary)\r\n* Med, Rx Total Allowed (Secondary)\r\n* Med, Rx Per Member Per Month (Secondary)\r\n* Short Term Disability Cost per Employee (Secondary)\r\n* Incidental Absence Days per Employee (Secondary)",
    'issues':[  
      {  
        'key':'PP-415',
        'summary':'Program Story Visuals',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-414',
        'summary':'Program story measures',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-413',
        'summary':'Impact Calculator ',
        'priority':'P3',
        'status':'new'
      }
    ],
    'length':3,
    'toDoNumber':3,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':'2019-07-31',
    'readinessStatus':'Not Started',
    'statusColor':'gray',
    'statusReason':'No items in the epic are in progress.'
  },
  {  
    'key':'PP-9',
    'summary':'Joins in app',
    'name':'Joins',
    'priority':'P3',
    'status':'new',
    'description':"Acceptance Criteria 5/8/19:\r\n* data should only pull from it's source table every time \r\n* you can break any measure down by any breakdown  \r\n* you can break any custom measure down by any breakdown\r\n* By default nulls are excluded from the summary measure and the breakdown measure \r\n* We need to test the summary # at the top as the aggregation of the rows vs. total dollars including nulls",
    'issues':[  
      {  
        'key':'PP-602',
        'summary':'UX to test the summary # at the top as the aggregation of the rows vs. total dollars including nulls',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-601',
        'summary':'By default nulls are excluded from the summary measure and the breakdown measure',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-600',
        'summary':'break any custom measure down by any breakdown',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-599',
        'summary':'Break any measure down by any breakdown',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-598',
        'summary':"Data should pull from it's source table every time ",
        'priority':'P3',
        'status':'new'
      }
    ],
    'length':5,
    'toDoNumber':5,
    'inProgressNumber':0,
    'doneNumber':0,
    'otherNumber':0,
    'dueDate':'2019-06-30',
    'readinessStatus':'Not Started',
    'statusColor':'gray',
    'statusReason':'No items in the epic are in progress.'
  },
  {  
    'key':'PP-7',
    'summary':'Condition Stories',
    'name':'Condition Stories',
    'priority':'P4',
    'status':'new',
    'description':'Mocks at\r\nhttps://projects.invisionapp.com/d/main#/console/14623254/316359768/preview',
    'issues':[  
      {  
        'key':'PP-231',
        'summary':'Change HCC PMPM box to Average Med Paid per HCC',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-213',
        'summary':'Angular Line Chart Component',
        'priority':'P3',
        'status':'indeterminate'
      },
      {  
        'key':'PP-208',
        'summary':'Condition story top section',
        'priority':'P4',
        'status':'new'
      },
      {  
        'key':'PP-207',
        'summary':'Condition Standard Stories List view',
        'priority':'P4',
        'status':'new'
      },
      {  
        'key':'PP-182',
        'summary':'Design and implementation for SS when a single (or multiple) requests fail ',
        'priority':'P4',
        'status':'new'
      },
      {  
        'key':'PP-179',
        'summary':'Opiod Use Standard Story (METRICS NOT IN ZEUS)',
        'priority':'P4',
        'status':'new'
      },
      {  
        'key':'PP-177',
        'summary':'Standard Stories Phase 2',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-165',
        'summary':'Maternity Standard Story',
        'priority':'P4',
        'status':'new'
      },
      {  
        'key':'PP-162',
        'summary':'Diabetes Standard Story',
        'priority':'P4',
        'status':'new'
      },
      {  
        'key':'PP-161',
        'summary':'Behavioral Health Standard Story',
        'priority':'P4',
        'status':'new'
      },
      {  
        'key':'PP-160',
        'summary':'Musculoskeletal Standard Story',
        'priority':'P4',
        'status':'new'
      },
      {  
        'key':'PP-159',
        'summary':'Elective Surgery Standard Story',
        'priority':'P4',
        'status':'new'
      },
      {  
        'key':'PP-158',
        'summary':'Cardiovascular Standard Story',
        'priority':'P4',
        'status':'new'
      },
      {  
        'key':'PP-157',
        'summary':'Neoplasms Standard Story',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-22',
        'summary':'Behavioral Health Services Research',
        'priority':'High',
        'status':'done'
      },
      {  
        'key':'PP-21',
        'summary':'Musculoskeletal Procedures Research',
        'priority':'High',
        'status':'done'
      },
      {  
        'key':'PP-20',
        'summary':'Sign off on Mocks',
        'priority':'P3',
        'status':'new'
      },
      {  
        'key':'PP-10',
        'summary':'Ability to download multiple Standard Stories at once ',
        'priority':'P3',
        'status':'new'
      }
    ],
    'length':18,
    'toDoNumber':15,
    'inProgressNumber':1,
    'doneNumber':2,
    'otherNumber':0,
    'dueDate':'2019-07-31',
    'readinessStatus':'In Progress',
    'statusColor':'rgb(97, 97, 235)',
    'statusReason':'In progress or partially complete.'
  }
]